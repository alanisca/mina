<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/comunidades/defensoresHidricos/images/bg.jpeg">
            <!-- <video id="bg" autoplay="false" loop="true" src="<?= media(); ?>islas/comunidades/defensoresHidricos/videos/bg01.mp4"></video> -->
            <!-- <video id="bg" autoplay="false" loop="true" src="<?= media(); ?>islas/comunidades/defensoresHidricos/videos/bg02.mp4"></video> -->

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/comunidades/defensoresHidricos/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="btn1" src="<?= media(); ?>islas/comunidades/defensoresHidricos/images/btn1.png">
            <img id="btn1_press" src="<?= media(); ?>islas/comunidades/defensoresHidricos/images/btn1_press.png">
            <img id="pleca" src="<?= media(); ?>islas/comunidades/defensoresHidricos/images/pleca.png">
            
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="healthcenter_liga" src="<?= media(); ?>islas/comunidades/healthcenter/images/liga.png">
            <img id="healthcenter_liga_press" src="<?= media(); ?>islas/comunidades/healthcenter/images/liga_press.png">
            <img id="parque_liga" src="<?= media(); ?>islas/comunidades/parque/images/liga.png">
            <img id="parque_liga_press" src="<?= media(); ?>islas/comunidades/parque/images/liga_press.png">
            <img id="finca_liga" src="<?= media(); ?>islas/comunidades/finca/images/liga.png">
            <img id="finca_liga_press" src="<?= media(); ?>islas/comunidades/finca/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-13.5 0 -4" scale="1 1 1" rotation="-15 70 0" load-obj="opciones">
        <a-image id="pleca" src="#pleca" width="1.18" height="1" geometry="width: 8.5; height: 1.55" material=""></a-image>
            <a-image id="btn01" id-video="persona01" src="#btn1" id-src="btn1" width="1.18" height="1" geometry="width: 7.66; height: 1.1" position="0 -0.74 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.77 0.6 0.1"></a-image>
            </a-image>
        </a-entity>
        
        <a-entity position="-6.3 5 7.6" rotation="0 130 0">
            <a-image src="#healthcenter_liga" id-src="healthcenter_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/healthcenter" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="0.4 4.5 -12.4" rotation="0 0 0">
            <a-image src="#parque_liga" id-src="parque_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/parque" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="5.5 5 -12" rotation="0 10 0">
            <a-image src="#finca_liga" id-src="finca_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/parque" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <!-- <a-videosphere id="sky01_360" src="#bg01" geometry="radius: 100;"></a-videosphere> -->
        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 28.67; thetaLength: 61.28; thetaStart: 72.76; phiStart: 217.8" material="color: #fcfcfc"></a-videosphere>
        <a-sky radius="100" id="sky" src="#bg" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/comunidades/defensoresHidricos.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>