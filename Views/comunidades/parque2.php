<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/comunidades/parque/images/bg.jpeg">

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/comunidades/parque/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/comunidades/parque/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona03" loop="false" src="<?= media(); ?>islas/comunidades/parque/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="mina_btn1" src="<?= media(); ?>islas/mina/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/mina/images/btn1_press.png">
            <img id="mina_pleca" src="<?= media(); ?>islas/mina/images/pleca.png">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="gigantes_liga" src="<?= media(); ?>islas/mina/gigantes/images/liga.png">
            <img id="gigantes_liga_press" src="<?= media(); ?>islas/mina/gigantes/images/liga_press.png">
            <img id="palaelectrica_liga" src="<?= media(); ?>islas/mina/palaelectrica/images/liga.png">
            <img id="palaelectrica_liga_press" src="<?= media(); ?>islas/mina/palaelectrica/images/liga_press.png">
            <img id="vuela_liga" src="<?= media(); ?>islas/mina/vuela/images/liga.png">
            <img id="vuela_liga_press" src="<?= media(); ?>islas/mina/vuela/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-7 1.7 -8.2" scale="1 1 1" rotation="0 40 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-4.5 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 0.6 0.4"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="0 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2 0.6 0.4"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona03" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="4.5 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 55.76; thetaLength: 82.4; thetaStart: 73.67; phiStart: 164.06" visible="" load-obj="" material=""></a-videosphere>
        
        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/lab.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>