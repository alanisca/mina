<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video regresar">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/comunidades/finca/images/bg.jpg" >
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/comunidades/finca/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
        </a-assets>

        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 26.85; thetaLength: 26.71; thetaStart: 79.43; phiStart: 121" material="color: #ffffff"></a-videosphere>
        <a-sky radius="100" src="#bg" load-obj="Sky"></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/comunidades/finca.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>