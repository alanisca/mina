<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/comunidades/caminos/images/bg.jpg">
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/comunidades/caminos/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/comunidades/caminos/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/comunidades/caminos/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona03" loop="false" src="<?= media(); ?>islas/comunidades/caminos/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="btn01" src="<?= media(); ?>islas/comunidades/caminos/images/btn1.png">
            <img id="btn01_press" src="<?= media(); ?>islas/comunidades/caminos/images/btn1_press.png">
            <img id="btn02" src="<?= media(); ?>islas/comunidades/caminos/images/btn2.png">
            <img id="btn02_press" src="<?= media(); ?>islas/comunidades/caminos/images/btn2_press.png">
            <img id="btn03" src="<?= media(); ?>islas/comunidades/caminos/images/btn3.png">
            <img id="btn03_press" src="<?= media(); ?>islas/comunidades/caminos/images/btn3_press.png">
            <img id="pleca" src="<?= media(); ?>islas/comunidades/caminos/images/pleca.png">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="iptcoclecito_liga" src="<?= media(); ?>islas/comunidades/iptcoclecito/images/liga.png">
            <img id="iptcoclecito_liga_press" src="<?= media(); ?>islas/comunidades/iptcoclecito/images/liga_press.png">
            <img id="testimonio_liga" src="<?= media(); ?>islas/comunidades/testimonio/images/liga.png">
            <img id="testimonio_liga_press" src="<?= media(); ?>islas/comunidades/testimonio/images/liga_press.png">
            <img id="ceibaFarm_liga" src="<?= media(); ?>islas/comunidades/ceibaFarm/images/liga.png">
            <img id="ceibaFarm_liga_press" src="<?= media(); ?>islas/comunidades/ceibaFarm/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-7 1.7 -8.2" scale="1 1 1" rotation="0 40 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
            <a-image id="btn01" id-video="persona01" src="#btn01" id-src="btn01" width="1.18" height="1" geometry="width: 4.32; height: 1.1" position="-5.37 -1 0.2" btn-click="Video" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.1 0.6 0.1"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#btn02" id-src="btn02" width="1.18" height="1" geometry="width: 5.51; height: 1.1" position="-0.3 -1 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 0.6 0.1"></a-image>
            </a-image>
            <a-image id="btn03" id-video="persona03" src="#btn03" id-src="btn03" width="1.18" height="1" geometry="width: 6.86; height: 1.1" position="6.04 -1 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-6.3 5 7.6" rotation="0 130 0">
            <a-image src="#iptcoclecito_liga" id-src="iptcoclecito_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/iptcoclecito" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="0.4 4.5 -12.4" rotation="0 0 0">
            <a-image src="#testimonio_liga" id-src="testimonio_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/iptcoclecito" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="0 -5 -12.4" rotation="0 0 0">
            <a-image src="#ceibaFarm_liga" id-src="ceibaFarm_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/comunidades/iptcoclecito" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 66.5; thetaLength: 32.3; thetaStart: 76.7; phiStart: 43.5" material="color: #ffffff; opacity: 0.54"></a-videosphere>
        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 26.9; thetaLength: 50.3; thetaStart: 74.33; phiStart: 145.98" visible="" load-obj="" material=""></a-videosphere>
        
        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/comunidades/caminos.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>