<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Mina Panama</title>
    <link href="Assets/css/plugins/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/loading.css" rel="stylesheet">
    <link href="Assets/css/home.css" rel="stylesheet">
    <script src="Assets/js/plugins/anime.js"></script>
    <!-- <script src="https://aframe.io/releases/0.8.0/aframe.min.js"></script> -->
    <script src="<?= media(); ?>js/plugins/aframe1.0.4.min.js"></script>
    <script src="Assets/js/plugins/aframe-orbit-controls.min.js"></script>
    <script src="<?= media(); ?>js/config.js"></script>
    <script src="<?= media(); ?>js/home/detect.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <script src="<?= media(); ?>js/360/video360.js"></script>
    <script src="<?= media(); ?>js/global.js"></script>
</head>
<body>
    <?php include('Views/temaplate/loading.php'); ?>
    <?php include('Views/temaplate/mainContain.php'); ?>
    
    <a-scene vr-mode-ui="enterVRButton: #vrmode;" load-obj="Scene" cursor="rayOrigin: mouse">
        <a-assets>
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="isla_comu_press" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_comu_press.png">
            <img id="isla_comu" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_comu.png">
            <img id="isla_medio_press" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_medio_press.png">
            <img id="isla_medio" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_medio.png">
            <img id="isla_mina_press" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_mina_press.png">
            <img id="isla_mina" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_mina.png">
            <img id="isla_proces_press" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_proces_press.png">
            <img id="isla_proces" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_proces.png">
            <img id="isla_puerto_press" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_puerto_press.png">
            <img id="isla_puerto" crossorigin="anonymous" src="<?= media(); ?>images/islas/isla_puerto.png">
            <img id="luz" crossorigin="anonymous" src="Assets/images/islas/luz.png">
            
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
            <!-- Modelos -->
            <!-- <a-asset-item id="isla_medioModel" src="<?= media(); ?>models/Test4/BakedTxt.gltf"></a-asset-item> -->
            
            <a-asset-item id="isla_comunidadesModel" src="<?= media(); ?>models/Comuniades_vecinas/isla.gltf" crossorigin="anonymous"></a-asset-item>
            <a-asset-item id="isla_medioModel" src="<?= media(); ?>models/Medio_Ambiente/isla.gltf" crossorigin="anonymous"></a-asset-item>
            <a-asset-item id="isla_minaModel" src="<?= media(); ?>models/Mina/isla.gltf" crossorigin="anonymous"></a-asset-item>
            <a-asset-item id="isla_procesamientoModel" src="<?= media(); ?>models/Procesamiento/isla.gltf" crossorigin="anonymous"></a-asset-item>
            <a-asset-item id="isla_puertoModel" src="<?= media(); ?>models/Puerto/isla.gltf" crossorigin="anonymous"></a-asset-item>
            <!-- Modelos -->
            <img id="bg-fija" src="<?= media(); ?>images/home/background/fija.png" crossorigin="anonymous">
            <img id="bg-giro1" src="<?= media(); ?>images/home/background/giro1.png" crossorigin="anonymous">
            <img id="bg-giro2" src="<?= media(); ?>images/home/background/giro2.png" crossorigin="anonymous">
            <img id="bg-giro3" src="<?= media(); ?>images/home/background/giro3.png" crossorigin="anonymous">
        </a-assets>
        <a-entity
            cursor="rayOrigin: mouse;"
            camera="active: true; fov: 80; zoom: 1;"
            fcamera
            id="camera"
            position="0 2 6"
            orbit-controls="
                autoRotate: false;
                target: #target;
                enableDamping: true;
                minPolarAngle: 1.2;
                maxPolarAngle: 1.2;
                rotateSpeed:0.25;
                enableKeys: false;
                enableZoom: false;
                autoVRLookCam: false;">
            <a-image id="luz" src="#luz" class="normalModeObj" position=".1 -2.7 -5" geometry="height:5; width:7"></a-image>
        </a-entity>
        
        <a-entity id="target">
            <a-entity class="isla_comuParent" position="2 0 -2.8">
                <a-entity id="modelVRIsla" class="isla_comu" gltf-model="#isla_comunidadesModel" position="0.5 0.7 0.65" scale="9 9 9" rotation="0 0.7 0" load-obj=""></a-entity>
                <a-image id="image-isla_comu" src="#isla_comu" id-src="isla_comu" position="-0.1 1.3 -0.1" width="1.18" height="1" scale="1 1 1" btn-click="" class="vr-button" vr-button="/Comunidades" liga="Comunidades"></a-image>
            </a-entity>

            <a-entity class="isla_medioParent" position="-2 0 -2.8">
                <a-entity id="modelVRIsla" class="isla_medio" gltf-model="#isla_medioModel" position="0.65 0.7 0.65" scale="9 9 9" rotation="0 0.7 0" load-obj=""></a-entity>
                <a-image id="image-isla_medio" src="#isla_medio" id-src="isla_medio" position="-0.1 1.3 -0.1" width="1" height="1" scale="1 1 1" btn-click="Liga" class="vr-button" vr-button="/medioambiente" liga="/medioambiente"></a-image>
            </a-entity>

            <a-entity class="isla_minaParent" position="-3 0 0.7">
                <a-entity id="modelVRIsla" class="isla_mina" gltf-model="#isla_minaModel" position="0.74241 0.7 0.8" scale="9 9 9" rotation="0 0.7 0" load-obj=""></a-entity>
                <a-image id="image-isla_mina" src="#isla_mina" id-src="isla_mina" position="-0.1 1.3 -0.1" width="1.19" height="1" scale="1 1 1" btn-click="Liga" class="vr-button" vr-button="/mina" liga="/mina"></a-image>
            </a-entity>

            <a-entity class="isla_procesParent" position="3 0 0.7">
                <a-entity id="modelVRIsla" class="isla_proces" gltf-model="#isla_procesamientoModel" position="0.65 0.7 0.8" scale="9 9 9" rotation="0 0.7 0" load-obj=""></a-entity>
                <a-image id="image-isla_proces" src="#isla_proces" id-src="isla_proces" position="-0.1 1.3 -0.1" width="1.2" height="1" scale="1 1 1" btn-click="Liga" class="vr-button" vr-button="/Procesamiento" liga="/Procesamiento"></a-image>
            </a-entity>

            <a-entity class="isla_puertoParent" position="0 0 3">
                <a-entity id="modelVRIsla" class="isla_puerto" gltf-model="#isla_puertoModel" position="0.6 0.7 0.6" scale="9 9 9" rotation="0 0.7 0" load-obj=""></a-entity>
                <a-image id="image-isla_puerto" src="#isla_puerto" id-src="isla_puerto" position="-0.1 1.3 -0.1" width="1.19" height="1" scale="1 1 1" btn-click="" class="vr-button" vr-button="/puerto" liga="/Puerto"></a-image>
            </a-entity>
        </a-entity>

        <?php include('Views/temaplate/menuVR.php'); ?>
        
        <!-- <a-videosphere radius="490" material=" src: #bg-fija; alphaTest: 0.1"></a-videosphere>
        <a-videosphere radius="490" animation="property: rotation; to: 0 360 0; loop: true; dur: 20000; easing:linear;" material="src: #bg-giro1; alphaTest: 0.3;"></a-videosphere>
        <a-videosphere radius="480" animation="property: rotation; to: 0 -360 0; loop: true; dur: 20000; easing:linear;" material="src: #bg-giro2; alphaTest: 0.3;"></a-videosphere>
        <a-videosphere radius="470" animation="property: rotation; to: 0 360 0; loop: true; dur: 20000; easing:linear;" material="src: #bg-giro3; alphaTest: 0.3;"></a-videosphere> -->
        <a-sky color="#BEC8D9" ></a-sky>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/plugins/preload_files.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>

    <script src="<?= media(); ?>js/home/rotacion.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/home/main.js"></script>
    <script>
        let init360 = () =>{
            console.log("Ready for use");
        }
    </script>
</body>
</html>