            <!-- VR -->
            <img id="punteroVR" src="<?= media(); ?>images/vr/punteroVR.png" crossorigin="anonymous">
            <img id="menuVr" src="<?= media(); ?>images/vr/menuVr.png" crossorigin="anonymous">
            <img id="menuVr_press" src="<?= media(); ?>images/vr/menuVr_press.png" crossorigin="anonymous">
            <!-- Menús VR -->
            <img id="menuVR1" src="<?= media(); ?>images/vr/menuVR1.png" crossorigin="anonymous">
            
            <img id="base_menuVR" src="<?= media(); ?>images/vr/base_menuVR.png" crossorigin="anonymous">
            <img id="menu_sound_on" src="<?= media(); ?>images/home/menu_sound_on.png" crossorigin="anonymous">
            <img id="menu_sound_on_press" src="<?= media(); ?>images/home/menu_sound_on_press.png" crossorigin="anonymous">
            <!-- Progreso VR -->
            <img id="menuVR_Progreso" src="<?= media(); ?>images/home/popup/menu/menuVR_Progreso.png" crossorigin="anonymous">
            <img id="plecaVR" src="<?= media(); ?>images/home/popup/bottomMenu/plecaVR.png" crossorigin="anonymous">
            <img id="stepVR" src="<?= media(); ?>images/home/popup/bottomMenu/step.png" crossorigin="anonymous">
            <img id="stepVR_press" src="<?= media(); ?>images/home/popup/bottomMenu/step_press.png" crossorigin="anonymous">
            <!-- Loading VR -->
            <img id="menuVR_islas" src="<?= media(); ?>images/home/popup/menu/islas.png" crossorigin="anonymous">
            <img id="menuVR_islas_press" src="<?= media(); ?>images/home/popup/menu/islas_press.png" crossorigin="anonymous">
            <img id="menuVR_salirvr" src="<?= media(); ?>images/home/popup/menu/salirvr.png" crossorigin="anonymous">
            <img id="menuVR_salirvr_press" src="<?= media(); ?>images/home/popup/menu/salirvr_press.png" crossorigin="anonymous">
            <img id="menuVR_how" src="<?= media(); ?>images/home/popup/menu/how.png" crossorigin="anonymous">
            <img id="menuVR_how_press" src="<?= media(); ?>images/home/popup/menu/how_press.png" crossorigin="anonymous">
            <img id="menuVR_reiniciar" src="<?= media(); ?>images/home/popup/menu/reiniciar.png" crossorigin="anonymous">
            <img id="menuVR_reiniciar_press" src="<?= media(); ?>images/home/popup/menu/reiniciar_press.png" crossorigin="anonymous">
            <img id="menuVR_acerca" src="<?= media(); ?>images/home/popup/menu/acerca.png" crossorigin="anonymous">
            <img id="menuVR_acerca_press" src="<?= media(); ?>images/home/popup/menu/acerca_press.png" crossorigin="anonymous">
            
            <img id="menuVR_compartir" src="<?= media(); ?>images/home/popup/menu/compartir.png" crossorigin="anonymous">
            <!-- Menús VR Redes -->
            <img id="menuVR_twitter" src="<?= media(); ?>images/home/popup/menu/twitter.png" crossorigin="anonymous">
            <img id="menuVR_twitter_press" src="<?= media(); ?>images/home/popup/menu/twitter_press.png" crossorigin="anonymous">
            <img id="menuVR_face" src="<?= media(); ?>images/home/popup/menu/face.png" crossorigin="anonymous">
            <img id="menuVR_face_press" src="<?= media(); ?>images/home/popup/menu/face_press.png" crossorigin="anonymous">
            <img id="menuVR_whats" src="<?= media(); ?>images/home/popup/menu/whats.png" crossorigin="anonymous">
            <img id="menuVR_whats_press" src="<?= media(); ?>images/home/popup/menu/whats_press.png" crossorigin="anonymous">
            <img id="menuVR_link" src="<?= media(); ?>images/home/popup/menu/link.png" crossorigin="anonymous">
            <img id="menuVR_link_press" src="<?= media(); ?>images/home/popup/menu/link_press.png" crossorigin="anonymous">