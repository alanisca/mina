    <div class="mainContain">
        <div class="mainlogo col-3 col-sm-2"></div>
        <div class="menu"></div>
        <div class="plecaContain">
            <div class="contain introduccion">
                <h5>
                    ¿Estás listo para conocer cómo se
                    produce el Cobre en una mina de clase
                    mundial? Preparado para observar uno
                    de los camiones más grandes del mundo
                    operados por audaces panameños v
                    descubrir cómo retribuimos al medio
                    ambiente y a la sociedad?
                </h5>
                <h3>
                    Bienvenido al Tour Virtual <br/>
                    de Cobre Panamá.
                </h3>
                <div class="comenzar"></div>
                <div class="comenzarVR"></div>
            </div>
            <div class="contain instrucciones">
                <div class="img ejemplo1"></div>
                <h5 class="instruccion1">
                    Arrastra sobre las islas para rotarlas
                </h5>
                <div class="img ejemplo2"></div>
                <h5 class="instruccion2">
                    Toca los botones sobre ellas o el indicador 
                    en la zona inferior para conocer más.
                </h5>
                <div class="entendido"></div>
            </div>
            <div class="contain menus">
                <div class="cerrar"></div>
                <div class="opt regresarIsla" style="display:none;"></div>
                <div class="opt salirVR" style="display:none;"></div>
                <div class="opt how"></div>
                <div class="opt instrucciones"></div>
                <div class="opt reiniciar"></div>
                <div class="opt acercade"></div>
                <div class="opt compartir"></div>
                <div class="opt redes">
                    <div class="twiter"></div>
                    <div class="facebook"></div>
                    <div class="whatsapp"></div>
                    <div class="compartir"></div>
                </div>
            </div>
        </div>
        <div class="regresar-islas col-3 col-sm-2"></div>
        <div class="regresar-atras col-1"></div>
        <div class="progresoGeneral">
            <h1 class="progreso">0</h1>
            <h1 class="signo">%</h1></br>
            <h1 class="texto">Progreso</h1>
        </div>
        <div class="bottomMenu">
            <div class="fondo"></div>
            <div class="boton">
                <div class="cerrarMenu"></div>
            </div>
            <h3>Etapa 3 - Operación</h3>
            <div class="lineaProgreso">
                <div class="step" data-index="1"></div>
                <div class="step" data-index="2"></div>
                <div class="step active" data-index="3"></div>
                <div class="step" data-index="4"></div>
                <hr>
            </div>
        </div>
        <div class="footer">
            <div class="text">
                © 2022 Copyright by _______
            </div>
            <div class="buttons">
                <div class="vrButton" id="vrmode"></div>
                <div class="vr" ></div>
                <div class="audioButton"></div>
            </div>
        </div>
    </div>