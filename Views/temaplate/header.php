    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= media(); ?>images/favicon.png">
    <title>Mina Panama</title>
    <link href="<?= media(); ?>css/plugins/bootstrap.min.css" rel="stylesheet">
    <link href="<?= media(); ?>css/loading.css" rel="stylesheet">
    <link href="<?= media(); ?>css/home.css" rel="stylesheet">
    <link href="<?= media(); ?>css/360videos.css" rel="stylesheet">
    <script src="<?= media(); ?>js/plugins/anime.js"></script>
    <script src="<?= media(); ?>js/plugins/aframe1.0.4.min.js"></script>
    <!-- <script src="https://aframe.io/releases/1.4.0/aframe.min.js"></script> -->
    <script src="<?= media(); ?>js/config.js"></script>
    <script src="<?= media(); ?>js/global.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@latest/pace-theme-default.min.css">
    <script src="<?= media(); ?>js/360/video360.js"></script>
