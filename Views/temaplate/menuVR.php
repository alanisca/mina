        <a-entity id="vrCameraParent" scale="1 1 1" rotation="0 90 0">
            <a-camera id="normalCamera" reverse-mouse-drag="true" cursor="rayOrigin: entity;">
                <!-- <a-cursor id="vrCursor" cursor="" visible="true"></a-cursor> -->
                <!-- <a-sky id="tapSky" radius="2" src="">
                    <a-image load-obj="" id="tap_continuarVR1" src="#tap_continuarVR" rotation="0 180 0" position="0 -0.05 -0.5" geometry="height: 0.027; width: 0.225"></a-image>
                </a-sky> -->
                <a-ring id="cursor-load" class="vrCursor"
                        position="0 0 -1"
                        geometry="primitive: ring; radiusInner: 0.038; radiusOuter: 0.05; thetaLength: 0" 
                        material="color: #49b6c0; shader: flat" load-obj>
                </a-ring>
                <a-ring load-obj position="0 0 -1" class="vrCursor"
                        color="#fff" 
                        radius-inner="0" 
                        radius-outer=".038" 
                        opacity=".5" 
                        material="" 
                        geometry="radiusInner: 0.038; radiusOuter: 0.05">
                </a-ring>
                <a-ring load-obj position="0 0 -1" class="vrCursor"
                        color="#fff" 
                        radius-inner="0" 
                        radius-outer=".02" 
                        opacity=".5" 
                        material="" 
                        geometry="radiusInner: 0.001; radiusOuter: 0.01">
                </a-ring>
            </a-camera>
            <a-entity load-obj id="menu" class="vrModeObj" visible="false">
                <a-entity position="0 -3 -2" rotation="315 0 0">
                    <a-image id="base_menuVR1" src="#base_menuVR" id-src="base_menuVR" position="0.2 0 -0.001" geometry="width: 1.4"></a-image>
                    <a-image load-obj id="menuVrButton" src="#menuVr" id-src="menuVr" btn-click="Menu"></a-image>
                    <a-image load-obj="" id="menu_sound_on2" src="#menu_sound_on" id-src="menu_sound_on" position="0.65 0 0" btn-click="Menu" geometry="height: 0.3; width: 0.3"></a-image>
                </a-entity>

                <a-image load-obj id="menuVR" src="#menuVR1" position="0 1 -2" geometry="height: 2; width: 1.8" scale=".0 .0 .0" visible="false">
                    <a-image load-obj id="progresVR" src="#menuVR_Progreso" position="0 1 0.01" material="" geometry="height: 0.35; width: 1.56">
                        <a-entity load-obj text="value: 0; wrapCount: 9; align: right; anchor: right" position="0.01 0.06 0.01"></a-entity>
                    </a-image>
                    <a-image load-obj src="#menuVR_islas" position="0 0.6 0.01"   geometry="height: 0.15" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_islas" vr-button></a-image>
                    <a-image load-obj src="#menuVR_salirvr" position="0 0.4 0.01" geometry="height: 0.15" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_salirvr" vr-button></a-image>
                    <a-image load-obj src="#menuVR_how" position="0 0.2 0.01"     geometry="height: 0.15" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_how" vr-button></a-image>
                    <a-image load-obj src="#menuVR_reiniciar" position="0 0 0.01" geometry="height: 0.15" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_reiniciar" vr-button></a-image>
                    <a-image load-obj src="#menuVR_acerca" position="0 -0.2 0.01" geometry="height: 0.15" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_acerca" vr-button></a-image>

                    <a-image load-obj src="#menuVR_compartir" position="0 -0.4 0.01"  geometry="height: 0.15" material="alphaTest: 0.5"></a-image>

                    <a-image load-obj src="#menuVR_twitter" position="-0.4 -0.6 0.01" geometry="height: 0.2; width: 0.2" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_twitter" vr-button></a-image>
                    <a-image load-obj src="#menuVR_face" position="-0.15 -0.6 0.01"   geometry="height: 0.2; width: 0.2" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_face" vr-button></a-image>
                    <a-image load-obj src="#menuVR_whats" position="0.15 -0.6 0.01"   geometry="height: 0.2; width: 0.2" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_whats" vr-button></a-image>
                    <a-image load-obj src="#menuVR_link" position="0.4 -0.6 0.01"     geometry="height: 0.2; width: 0.2" material="alphaTest: 0.5" btn-click="MenuBoton" id-src="menuVR_link" vr-button></a-image>
                </a-image>
            </a-entity>
        </a-entity>
