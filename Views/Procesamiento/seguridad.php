<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/seguridad/images/bg.jpeg">
            <video  id="yahir01" loop="false" src="<?= media(); ?>islas/procesos/seguridad/videos/yahir01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="yahir02" loop="false" src="<?= media(); ?>islas/procesos/seguridad/videos/yahir02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/procesos/seguridad/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="seguridad_btn1" src="<?= media(); ?>islas/procesos/seguridad/images/btn1.png">
            <img id="seguridad_btn1_press" src="<?= media(); ?>islas/procesos/seguridad/images/btn1_press.png">
            <img id="seguridad_btn2" src="<?= media(); ?>islas/procesos/seguridad/images/btn2.png">
            <img id="seguridad_btn2_press" src="<?= media(); ?>islas/procesos/seguridad/images/btn2_press.png">
            <img id="seguridad_tema" src="<?= media(); ?>islas/procesos/seguridad/images/tema.png">

            <img id="molienda_liga" src="<?= media(); ?>islas/procesos/molienda/images/liga.png">
            <img id="molienda_liga_press" src="<?= media(); ?>islas/procesos/molienda/images/liga_press.png">
            <img id="eppmina_liga" src="<?= media(); ?>islas/procesos/eppmina/images/liga.png">
            <img id="eppmina_liga_press" src="<?= media(); ?>islas/procesos/eppmina/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">
            
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
        
        <a-entity id="opciones" position="-0.9 1.5 5.6" scale="0.6 0.6 0.6" rotation="0 155.95 0" load-obj="">
            <a-image id="pleca" src="#seguridad_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93" material=""></a-image>
            
            <a-image id="btn01" src="#seguridad_btn1" id-src="seguridad_btn1" width="1.18" height="1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -1 0.2" btn-click="Video" id-video="yahir01">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 0.6 0.4"></a-image>
            </a-image>
            <a-image id="btn02" src="#seguridad_btn2" id-src="seguridad_btn2" width="1.18" height="1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="2.5 -1 0.2" btn-click="Video" id-video="yahir02">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-videosphere id="persona" src="#yahir01" geometry="radius: 99.5; phiLength: 36.63; thetaLength: 76.28; thetaStart: 67.49; phiStart: 137.85" material=""></a-videosphere>
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 41.52; thetaLength: 50.58; thetaStart: 78.31; phiStart: 176.6" material=""></a-videosphere>
      
        <a-entity position="-6.5 3.5 6" rotation="0 140 0">
            <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/Procesamiento/molienda">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="4.7 2.3 6.1" rotation="0 220 0">
            <a-image src="#eppmina_liga" id-src="eppmina_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" position="" btn-click="Liga" liga="/Procesamiento/eppmina">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.85 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-sky radius="100" src="#bg" color="" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/seguridad.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>