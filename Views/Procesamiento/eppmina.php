<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/eppmina/images/bg.jpeg">
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/procesos/eppmina/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop02" loop="true" src="<?= media(); ?>islas/procesos/eppmina/videos/loop02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="centroControl_liga" src="<?= media(); ?>islas/procesos/centroControl/images/liga.png">
            <img id="centroControl_liga_press" src="<?= media(); ?>islas/procesos/centroControl/images/liga_press.png">
            <img id="flotacion_liga" src="<?= media(); ?>islas/procesos/flotacion/images/liga.png">
            <img id="flotacion_liga_press" src="<?= media(); ?>islas/procesos/flotacion/images/liga_press.png">
            <img id="seguridad_liga" src="<?= media(); ?>islas/procesos/seguridad/images/liga.png">
            <img id="seguridad_liga_press" src="<?= media(); ?>islas/procesos/seguridad/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets> 
        
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 36.6; thetaLength: 29.2; thetaStart: 81.5; phiStart: 326.36" visible="" material="" load-obj=""></a-videosphere>
        <a-videosphere id="loop02_V" src="#loop02" geometry="radius: 99; phiLength: 97.92; thetaLength: 70.81; thetaStart: 73.71; phiStart: 16.51" visible="" material="" load-obj=""></a-videosphere>
        
        <a-entity position="-0.8 6.4 13.3" rotation="0 185 0">
            <a-image src="#centroControl_liga" id-src="centroControl_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/Procesamiento/centroControl">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="10.5 2.3 1.6" rotation="0 270 0" visible="">
            <a-image src="#flotacion_liga" id-src="flotacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/Procesamiento/flotacion">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-0.25 3.6 -10.5" rotation="">
            <a-image src="#seguridad_liga" id-src="seguridad_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" position="" btn-click="Liga" liga="/Procesamiento/seguridad">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/eppmina.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
            document.querySelector("#loop02_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>