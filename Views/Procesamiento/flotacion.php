<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/flotacion/images/bg.jpg">
            <video  id="olinda01" loop="false" src="<?= media(); ?>islas/procesos/flotacion/videos/olinda01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/procesos/flotacion/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop02" loop="true" src="<?= media(); ?>islas/procesos/flotacion/videos/loop02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop03" loop="true" src="<?= media(); ?>islas/procesos/flotacion/videos/loop03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="flotacion_btn1" src="<?= media(); ?>islas/procesos/flotacion/images/btn1.png">
            <img id="flotacion_btn1_press" src="<?= media(); ?>islas/procesos/flotacion/images/btn1_press.png">
            <img id="flotacion_tema" src="<?= media(); ?>islas/procesos/flotacion/images/tema.png">
            
            <img id="seguridad_liga" src="<?= media(); ?>islas/procesos/seguridad/images/liga.png">
            <img id="seguridad_liga_press" src="<?= media(); ?>islas/procesos/seguridad/images/liga_press.png">
            <img id="molienda_liga" src="<?= media(); ?>islas/procesos/molienda/images/liga.png">
            <img id="molienda_liga_press" src="<?= media(); ?>islas/procesos/molienda/images/liga_press.png">
            <img id="drone_liga" src="<?= media(); ?>islas/procesos/drone/images/liga.png">
            <img id="drone_liga_press" src="<?= media(); ?>islas/procesos/drone/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
        
        <a-entity id="opciones" position="4.4 0 -5.5" scale="0.6 0.6 0.6" rotation="0 330 0" load-obj="opciones">
            <a-image id="pleca" src="#flotacion_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
            <a-image id="btn02" src="#flotacion_btn1" id-src="flotacion_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="0 -1 0" btn-click="Video" id-video="olinda01">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-videosphere id="persona" src="#olinda01" geometry="radius: 99.5; phiLength: 25; thetaLength: 55.28; thetaStart: 80.45; phiStart: 260.8" visible="" load-obj="" material="color: #ededed"></a-videosphere>
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 59.55; thetaLength: 31.73; thetaStart: 107.4; phiStart: 179.83" visible="" load-obj="" material="color: #ededed"></a-videosphere>
        <a-videosphere id="loop02_V" src="#loop02" geometry="radius: 99; phiLength: 32.31; thetaLength: 44.53; thetaStart: 98.86; phiStart: 327.64" visible="" load-obj="" material="color: #dbdbdb"></a-videosphere>
        <a-videosphere id="loop03_V" src="#loop03" geometry="radius: 99; phiLength: 35.75; thetaLength: 12.49; thetaStart: 102.67; phiStart: 39.95" visible="" load-obj="" material="color: #ededed"></a-videosphere>
      
        <a-entity position="10.2 3.3 3.7" rotation="0 239 0">
            <a-image src="#seguridad_liga" id-src="seguridad_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/Procesamiento/seguridad" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-6.3 4 1.8" rotation="0 110 0">
            <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/Procesamiento/molienda" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="12.2 3 0" rotation="0 285 0">
            <a-image src="#drone_liga" id-src="drone_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12"  btn-click="Liga" liga="/Procesamiento/drone">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="#f7f7f7" load-obj="sky"></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/flotacion.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
            document.querySelector("#loop02_V").components.material.data.src.currentTime = 1;
            document.querySelector("#loop03_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>