<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video regresar">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/centroControl/images/bg.jpg" >
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/procesos/centroControl/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <video  id="loop02" loop="true" src="<?= media(); ?>islas/procesos/centroControl/videos/loop02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <video  id="loop03" loop="true" src="<?= media(); ?>islas/procesos/centroControl/videos/loop03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>

        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 134.15; thetaLength: 25.02; thetaStart: 89.41; phiStart: 96.68" material="color: #d6d6d6"></a-videosphere>
        <a-videosphere id="loop02_V" src="#loop02" geometry="radius: 99; phiLength: 50.47; thetaLength: 39.83; thetaStart: 82.98; phiStart: 244.2" material="color: #d6d6d6" ></a-videosphere>
        <a-videosphere id="loop03_V" src="#loop03" geometry="radius: 99; phiLength: 20.19; thetaLength: 26.85; thetaStart: 90.22; phiStart: 72.17" material="color: #d6d6d6" ></a-videosphere>
        <a-sky radius="100" src="#bg" load-obj="Sky"></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/centroControl.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
            document.querySelector("#loop02_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>