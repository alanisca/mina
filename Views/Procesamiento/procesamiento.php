<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video" cursor="rayOrigin: mouse;">
        <a-assets>
            <?php include('Views/temaplate/assets2D.php'); ?>
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/images/bg.jpeg" crossorigin="anonymous">
            <video  id="francisco1" loop="false" src="<?= media(); ?>islas/procesos/videos/francisco01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <video  id="francisco2" loop="false" src="<?= media(); ?>islas/procesos/videos/francisco02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <video  id="loop"       loop="true"  src="<?= media(); ?>islas/procesos/videos/loop.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png" crossorigin="anonymous">

            <img id="procesos_btn1" src="<?= media(); ?>islas/procesos/images/btn1.png" crossorigin="anonymous">
            <img id="procesos_btn1_press" src="<?= media(); ?>islas/procesos/images/btn1_press.png" crossorigin="anonymous">
            <img id="procesos_btn2" src="<?= media(); ?>islas/procesos/images/btn2.png" crossorigin="anonymous">
            <img id="procesos_btn2_press" src="<?= media(); ?>islas/procesos/images/btn2_press.png" crossorigin="anonymous">
            <img id="procesos_tema" src="<?= media(); ?>islas/procesos/images/tema.png" crossorigin="anonymous">

            <img id="molienda_liga" src="<?= media(); ?>islas/procesos/molienda/images/liga.png" crossorigin="anonymous">
            <img id="molienda_liga_press" src="<?= media(); ?>islas/procesos/molienda/images/liga_press.png" crossorigin="anonymous">
            <img id="relaves_liga" src="<?= media(); ?>islas/procesos/relaves/images/liga.png" crossorigin="anonymous">
            <img id="relaves_liga_press" src="<?= media(); ?>islas/procesos/relaves/images/liga_press.png" crossorigin="anonymous">

        </a-assets>
        
        <a-entity id="opciones" position="-6 0 .6" scale=".6 .6 .6" rotation="0 80 0" load-obj="aaopciones">
            <a-image id="pleca" src="#procesos_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93"></a-image>
            <a-image id="btn01" id-video="francisco1" src="#procesos_btn1" id-src="procesos_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 0.6 0.4"></a-image>
            </a-image>
            <a-image id="btn02" id-video="francisco2" src="#procesos_btn2" id-src="procesos_btn2" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="2.5 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 0.6 0.4"></a-image>
            </a-image>
        </a-entity>

        <?php include('Views/temaplate/menuVR.php'); ?>

        <a-videosphere id="persona" src="#francisco1" geometry="radius: 99; phiLength: 52.12; thetaLength: 81.87; thetaStart: 64.07; phiStart: 189.02"></a-videosphere>
        <a-videosphere id="banda" src="#loop" geometry="radius: 99.5; phiLength: 133.08; thetaLength: 57.37; thetaStart: 93.44; phiStart: 48.68"></a-videosphere>
      
        <a-entity position="-10 5.77 4.17341" rotation="0 95 0">
            <a-image src="#molienda_liga" id-src="molienda_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/Procesamiento/molienda">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.6 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-10 6.3 -4.2" rotation="0 85 0">
            <a-image src="#relaves_liga" id-src="relaves_liga" width="1.18" height="1" geometry="width: 5.17; height: 3.18" btn-click="Liga" liga="/Procesamiento/relaves" material="" class="activeVR">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.5 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-sky radius="100" src="#bg"></a-sky>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="Assets/js/360/procesamiento/procesamiento.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
            document.querySelector("#banda").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>