<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/procesos/relaves/images/bg.jpg">

            <!-- PERSONA(S) -->
            <!-- <video  id="persona01" loop="false" src="<?= media(); ?>islas/procesos/relaves/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video> -->

            <!-- OPCION MULTIPLE -->
            <!-- <img id="mina_btn1" src="<?= media(); ?>islas/procesos/relaves/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/procesos/relaves/images/btn1_press.png">
            <img id="mina_pleca" src="<?= media(); ?>islas/procesos/relaves/images/pleca.png"> -->
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="areanorte_liga" src="<?= media(); ?>islas/procesos/areanorte/images/liga.png">
            <img id="areanorte_liga_press" src="<?= media(); ?>islas/procesos/areanorte/images/liga_press.png">

            <img id="vuelarelave_liga" src="<?= media(); ?>islas/procesos/vuelarelave/images/liga.png">
            <img id="vuelarelave_liga_press" src="<?= media(); ?>islas/procesos/vuelarelave/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       

        <!-- areanorte -->
        <a-entity position="3.5 2.2 -11.6" rotation="0 -25 0">
            <a-image src="#areanorte_liga" id-src="areanorte_liga" width="1.18" height="1" geometry="width: 3.93; height: 3.13" btn-click="Liga" liga="/medioambiente/pasosfauna" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <!-- vuelarelave -->
        <a-entity position="-4 3.3 -11.6" rotation="0 10 0">
            <a-image src="#vuelarelave_liga" id-src="vuelarelave_liga" width="1.18" height="1" geometry="width: 6.18; height: 3.12" material="" btn-click="Liga" liga="/medioambiente/medioambiente" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.9 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/relaves.js"></script>
    <script>
        // let init360 = () => {
        //     document.querySelector("#persona").components.material.data.src.currentTime = 0;
        // }
    </script>
</body>
</html>