<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/procesos/molienda/images/bg.jpeg">
            <video  id="video01" loop="false" src="<?= media(); ?>islas/procesos/molienda/videos/olinda01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/procesos/molienda/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop02" loop="true" src="<?= media(); ?>islas/procesos/molienda/videos/loop02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop03" loop="true" src="<?= media(); ?>islas/procesos/molienda/videos/loop03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="molienda_btn1" src="<?= media(); ?>islas/procesos/molienda/images/btn1.png">
            <img id="molienda_btn1_press" src="<?= media(); ?>islas/procesos/molienda/images/btn1_press.png">
            <img id="molienda_tema" src="<?= media(); ?>islas/procesos/molienda/images/tema.png">
            
            <img id="liga" src="<?= media(); ?>islas/procesos/images/liga.png">
            <img id="liga_press" src="<?= media(); ?>islas/procesos/images/liga_press.png">
            <img id="centroControl_liga" src="<?= media(); ?>islas/procesos/centroControl/images/liga.png">
            <img id="centroControl_liga_press" src="<?= media(); ?>islas/procesos/centroControl/images/liga_press.png">
            <img id="flotacion_liga" src="<?= media(); ?>islas/procesos/flotacion/images/liga.png">
            <img id="flotacion_liga_press" src="<?= media(); ?>islas/procesos/flotacion/images/liga_press.png">
            
            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>

        
        <a-entity id="opciones" position="5.83898 0.37244 2.5499" scale="0.6 0.6 0.6" rotation="0 262.6000000000001 0" load-obj="">
            <a-image id="pleca" src="#molienda_tema" width="1.18" height="1" geometry="width: 8.03; height: 1.93" material=""></a-image>
            <a-image id="btn01" src="#molienda_btn1" id-src="molienda_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="0 -1 0.5" btn-click="Video" id-video="video01" material="" text__molienda="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-videosphere id="persona" src="#video01" geometry="radius: 99.5; phiLength: 33.2; thetaLength: 73.6; thetaStart: 72.5; phiStart: 333.7" visible="" load-obj=""></a-videosphere>
        <a-videosphere id="loop01_V" src="" geometry="radius: 99; phiLength: 25.8; thetaLength: 21.6; thetaStart: 86.5; phiStart: 2.6" visible="" material="" load-obj=""></a-videosphere>
        <a-videosphere id="loop02_V" src="" geometry="radius: 99; phiLength: 26.64; thetaLength: 16.1; thetaStart: 84.6; phiStart: 310.66" visible="" material="" load-obj=""></a-videosphere>
        <a-videosphere id="loop03_V" src="" geometry="radius: 99; phiLength: 41.3; thetaLength: 43.6; thetaStart: 84.3; phiStart: 38.3" visible="" material="" load-obj=""></a-videosphere>
      
        <a-entity position="8.5 5.8 -2" rotation="0 270 0">
            <a-image src="#liga" id-src="liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/Procesamiento">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.6 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="8.5 4.2 8.5" rotation="0 235 0">
            <a-image src="#flotacion_liga" id-src="flotacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/Procesamiento/flotacion">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="1.25 2.5 10.6" rotation="0 180 0">
            <a-image src="#centroControl_liga" id-src="centroControl_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/Procesamiento/centroControl">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/procesamiento/molienda.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 0;
            document.querySelector("#loop02_V").components.material.data.src.currentTime = 0;
            document.querySelector("#loop03_V").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>