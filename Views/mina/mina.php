<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/mina/images/bg.jpg">

            <video  id="persona01" loop="false" src="<?= media(); ?>islas/mina/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/mina/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="mina_btn1" src="<?= media(); ?>islas/mina/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/mina/images/btn1_press.png">
            <img id="mina_btn2" src="<?= media(); ?>islas/mina/images/btn2.png">
            <img id="mina_btn2_press" src="<?= media(); ?>islas/mina/images/btn2_press.png">
            <img id="mina_pleca" src="<?= media(); ?>islas/mina/images/tema.png">
            
            <img id="gigantes_liga" src="<?= media(); ?>islas/mina/gigantes/images/liga.png">
            <img id="gigantes_liga_press" src="<?= media(); ?>islas/mina/gigantes/images/liga_press.png">
            <img id="palaelectrica_liga" src="<?= media(); ?>islas/mina/palaelectrica/images/liga.png">
            <img id="palaelectrica_liga_press" src="<?= media(); ?>islas/mina/palaelectrica/images/liga_press.png">
            <img id="vuela_liga" src="<?= media(); ?>islas/mina/vuela/images/liga.png">
            <img id="vuela_liga_press" src="<?= media(); ?>islas/mina/vuela/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-6.5 1.6 7" scale="1 1 1" rotation="0 130 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 8; height: 1.65" material=""></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-2.5 -0.7 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 0.5 0.1"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#mina_btn2" id-src="mina_btn2" width="1.18" height="1" geometry="width: 4.62; height: 1.1" position="2.5 -0.7 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-videosphere id="persona" src="#persona02" geometry="radius: 98; phiLength: 66.85; thetaLength: 104.44; thetaStart: 41.64; phiStart: 142.72" visible="" load-obj="" material="color: #ededed"></a-videosphere>
        <a-entity position="0.4 4.5 -12.4" rotation="0 -10 0">
            <a-image src="#gigantes_liga" id-src="gigantes_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/mina/gigantes" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-3.2 5 -8.5" rotation="0 20 0">
            <a-image src="#palaelectrica_liga" id-src="palaelectrica_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/mina/palaelectrica" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-9.6 7.2 -8.9" rotation="0 50 0">
            <a-image src="#vuela_liga" id-src="vuela_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/mina/vuela">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="#e7edf3" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/mina/mina.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>