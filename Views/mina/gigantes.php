<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/mina/gigantes/images/bg.jpeg">

            <video  id="persona01" loop="false" src="<?= media(); ?>islas/mina/gigantes/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/mina/gigantes/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>

            <img id="mina_btn1" src="<?= media(); ?>islas/mina/gigantes/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/mina/gigantes/images/btn1_press.png">
            <img id="mina_btn2" src="<?= media(); ?>islas/mina/gigantes/images/btn2.png">
            <img id="mina_btn2_press" src="<?= media(); ?>islas/mina/gigantes/images/btn2_press.png">
            <img id="mina_tema" src="<?= media(); ?>islas/mina/gigantes/images/tema.png">
            
            <img id="camion_liga" src="<?= media(); ?>islas/mina/camion/images/liga.png">
            <img id="camion_liga_press" src="<?= media(); ?>islas/mina/camion/images/liga_press.png">
            <img id="simulacion_liga" src="<?= media(); ?>islas/mina/simulacion/images/liga.png">
            <img id="simulacion_liga_press" src="<?= media(); ?>islas/mina/simulacion/images/liga_press.png">
            

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
        <a-entity id="opciones" position="-7 1.7 -8.2" scale="1 1 1" rotation="0 52 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_tema" width="1.18" height="1" geometry="width: 8.02; height: 1.55"></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.02; height: 1.1" position="-3.3 -.7 0.34" btn-click="Video"></a-image>
            <a-image id="btn02" id-video="persona02" src="#mina_btn2" id-src="mina_btn2" width="1.18" height="1" geometry="width: 5.71; height: 1.1" position="1.8 -.7 0.31" btn-click="Video"></a-image>
        </a-entity>
        <a-videosphere id="persona" src="#persona02" geometry="radius: 99.5; phiLength: 30.73; thetaLength: 59.7; thetaStart: 74; phiStart: 163" material="color: #ededed"></a-videosphere>

        <a-entity position="8.1 6.9 1.3" rotation="0 270 0">
            <a-image src="#camion_liga" id-src="camion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/mina/camion"></a-image>
        </a-entity>
        <a-entity position="8 5.3 -10" rotation="0 315 0">
            <a-image src="#simulacion_liga" id-src="simulacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/mina/simulacion"></a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="#e7edf3" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/mina/gigantes.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
            
        }
    </script>
</body>
</html>