<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/mina/simulacion/images/bg.jpeg">
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/mina/simulacion/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/mina/simulacion/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/mina/simulacion/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="simulacion_btn1" src="<?= media(); ?>islas/mina/simulacion/images/btn1.png">
            <img id="simulacion_btn1_press" src="<?= media(); ?>islas/mina/simulacion/images/btn1_press.png">
            <img id="simulacion_btn2" src="<?= media(); ?>islas/mina/simulacion/images/btn2.png">
            <img id="simulacion_btn2_press" src="<?= media(); ?>islas/mina/simulacion/images/btn2_press.png">
            <img id="simulacion_tema" src="<?= media(); ?>islas/mina/simulacion/images/pleca.png">
            
            <img id="liga" src="<?= media(); ?>islas/mina/images/liga.png">
            <img id="liga_press" src="<?= media(); ?>islas/mina/images/liga_press.png">
            
            

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
        <a-entity id="opciones" position="-4.549 -0.5 -2.7" scale="0.6 0.6 0.6" rotation="0 70 0" load-obj="">
            <a-image id="pleca" src="#simulacion_tema" width="1.18" height="1" geometry="width: 9.02; height: 1.93"></a-image>
            <a-image id="btn01" src="#simulacion_btn1" id-src="simulacion_btn1" width="1.18" height="1" geometry="width: 6.65; height: 1.1" position="-2.9 -.7 0.2" btn-click="Video" id-video="persona01" material="" text__molienda=""></a-image>
            <a-image id="btn02" src="#simulacion_btn2" id-src="simulacion_btn2" width="1.18" height="1" geometry="width: 5.71; height: 1.1" position="3.4 -.7 0.2" btn-click="Video" id-video="persona02" material="" text__molienda=""></a-image>
        </a-entity>
        <a-videosphere id="persona" src="#persona01" geometry="radius: 98; phiLength: 41.6; thetaLength: 84.5; thetaStart: 66.8; phiStart: 129" material="color: #ffffff"></a-videosphere>
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 75.13; thetaLength: 65.18; thetaStart: 70.39; phiStart: 172.03" material="" visible=""></a-videosphere>
      
        <a-entity position="-9 5 -6" rotation="0 75 0">
            <a-image src="#liga" id-src="liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/Mina"></a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/mina/simulacion.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>