<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/medioambiente/playa/images/bg.jpg">

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/medioambiente/playa/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="mina_btn1" src="<?= media(); ?>islas/medioambiente/playa/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/medioambiente/playa/images/btn1_press.png">
            <img id="mina_pleca" src="<?= media(); ?>islas/medioambiente/playa/images/pleca.png">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="pasosFauna_liga" src="<?= media(); ?>islas/medioambiente/pasosfauna/images/liga.png">
            <img id="pasosFauna_liga_press" src="<?= media(); ?>islas/medioambiente/pasosfauna/images/liga_press.png">

            <img id="reforestacion_liga" src="<?= media(); ?>islas/medioambiente/reforestacion/images/liga.png">
            <img id="reforestacion_liga_press" src="<?= media(); ?>islas/medioambiente/reforestacion/images/liga_press.png">

            <img id="pitoffice_liga" src="<?= media(); ?>islas/medioambiente/pitoffice/images/liga.png">
            <img id="pitoffice_liga_press" src="<?= media(); ?>islas/medioambiente/pitoffice/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="8 -25 -5.5" scale="1 1 1" rotation="-10 300 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="8.5" height="1.54"></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="6.13" height="1.1" position="0 -0.65 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3 0.4 .1"></a-image>
            </a-image>
        </a-entity>
        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 43.46; thetaLength: 68.24; thetaStart: 77.36; phiStart: 349.02" visible="" load-obj="" material=""></a-videosphere>

        <!-- Paso Fauns -->
        <a-entity position="3.5 -20 -11.6" rotation="0 -25 0">
            <a-image src="#pasosFauna_liga" id-src="pasosFauna_liga" width="1.18" height="1" geometry="width: 3.93; height: 3.13" btn-click="Liga" liga="/medioambiente/pasosfauna" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <!-- Reforestacion -->
        <a-entity position="-4 -20 -11.6" rotation="0 30 0">
            <a-image src="#reforestacion_liga" id-src="reforestacion_liga" width="1.18" height="1" geometry="width: 3.92; height: 3.12" material="" btn-click="Liga" liga="/medioambiente/medioambiente" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <!-- pitoffice  -->
        <a-entity position="-9.6 -20 -8.9" rotation="0 42.4 0">
            <a-image src="#pitoffice_liga" id-src="pitoffice_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" material="" btn-click="Liga" liga="/medioambiente/pitoffice" class="">
            <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.2 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/playa.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>