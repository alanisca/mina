<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/medioambiente/pitoffice/images/bg.jpeg">

            <video  id="persona01" loop="false" src="<?= media(); ?>islas/medioambiente/pitoffice/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/medioambiente/pitoffice/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="btn1" src="<?= media(); ?>islas/medioambiente/pitoffice/images/btn1.png">
            <img id="btn1_press" src="<?= media(); ?>islas/medioambiente/pitoffice/images/btn1_press.png">
            <img id="btn2" src="<?= media(); ?>islas/medioambiente/pitoffice/images/btn2.png">
            <img id="btn2_press" src="<?= media(); ?>islas/medioambiente/pitoffice/images/btn2_press.png">
            <img id="pleca" src="<?= media(); ?>islas/medioambiente/pitoffice/images/pleca.png">

            <!-- LIGAS A OTROS SITIOS -->
            <img id="playa_liga" src="<?= media(); ?>islas/medioambiente/playa/images/liga.png">
            <img id="playa_liga_press" src="<?= media(); ?>islas/medioambiente/playa/images/liga_press.png">

            <img id="nursery_liga" src="<?= media(); ?>islas/medioambiente/nursery/images/liga.png">
            <img id="nursery_liga_press" src="<?= media(); ?>islas/medioambiente/nursery/images/liga_press.png">

            <img id="rio_liga" src="<?= media(); ?>islas/medioambiente/rio/images/liga.png">
            <img id="rio_liga_press" src="<?= media(); ?>islas/medioambiente/rio/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-7 0 -8.2" scale="1 1 1" rotation="-15 55 0" load-obj="opciones">
            <a-image id="pleca" src="#pleca" width="1.18" height="1" geometry="width: 11.02; height: 1.55"></a-image>
            <a-image id="btn01" id-video="persona01" src="#btn1" id-src="btn1" width="1.18" height="1" geometry="width: 4.78; height: 1.1" position="-3.4 -0.65 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.4 0.6 0.1"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#btn2" id-src="btn2" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="2.5 -0.65 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.6 0.1"></a-image>
            </a-image>
        </a-entity>

        <a-entity position="-5 2.5 10" rotation="0 140 0">
            <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="" liga="/medioambiente/playa" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="3 2.5 10" rotation="0 200 0">
            <a-image src="#nursery_liga" id-src="nursery_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="" liga="/medioambiente/nursery" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="0 5 -10" rotation="0 20 0">
            <a-image src="#rio_liga" id-src="rio_liga" width="1.18" height="1" geometry="width: 5.63; height: 3.12" btn-click="" liga="/medioambiente/rio" material="" class="activeVR" position="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 1.4 0.1"></a-image>
            </a-image>
        </a-entity>

        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 48.810; thetaLength: 87.130; thetaStart: 55.790; phiStart: 152.21" visible="" load-obj="" material=""></a-videosphere>
        
        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/pitoffice.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>