<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <img id="bg" src="<?= media(); ?>islas/medioambiente/nursery/images/bg.jpeg">

            <video  id="persona01" loop="false" src="<?= media(); ?>islas/medioambiente/nursery/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/medioambiente/nursery/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <img id="btn1" src="<?= media(); ?>islas/medioambiente/nursery/images/btn1.png">
            <img id="btn1_press" src="<?= media(); ?>islas/medioambiente/nursery/images/btn1_press.png">
            <img id="btn2" src="<?= media(); ?>islas/medioambiente/nursery/images/btn2.png">
            <img id="btn2_press" src="<?= media(); ?>islas/medioambiente/nursery/images/btn2_press.png">
            <img id="pleca" src="<?= media(); ?>islas/medioambiente/nursery/images/pleca.png">
            
            <img id="pitoffice_liga" src="<?= media(); ?>islas/medioambiente/pitoffice/images/liga.png">
            <img id="pitoffice_liga_press" src="<?= media(); ?>islas/medioambiente/pitoffice/images/liga_press.png">

            <img id="lab_liga" src="<?= media(); ?>islas/medioambiente/lab/images/liga.png">
            <img id="lab_liga_press" src="<?= media(); ?>islas/medioambiente/lab/images/liga_press.png">

            <img id="rio_liga" src="<?= media(); ?>islas/medioambiente/rio/images/liga.png">
            <img id="rio_liga_press" src="<?= media(); ?>islas/medioambiente/rio/images/liga_press.png">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-7.75 0 7" scale="1 1 1" rotation="-15 140 0" load-obj="opciones">
            <a-image id="pleca" src="#pleca" width="1.18" height="1" geometry="width: 7.57; height: 2.08"></a-image>
            <a-image id="btn01" id-video="persona01" src="#btn1" id-src="btn1" width="1.18" height="1" geometry="width: 3.96; height: 1.1" position="-3 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2 0.6 0.1"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#btn2" id-src="btn2" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="2.3 -1 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.2 0.6 0.1"></a-image>
            </a-image>
        </a-entity>

        <a-entity position="-5 2.5 10" rotation="0 140 0">
            <a-image src="#pitoffice_liga" id-src="pitoffice_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="" liga="/medioambiente/pitoffice" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="4.1 3.2 7.8" rotation="0 200 0">
            <a-image src="#lab_liga" id-src="lab_liga" width="1.18" height="1" geometry="width: 6.31; height: 3.12" btn-click="" liga="/medioambiente/lab" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.01 1.5 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="0 5 -10" rotation="0 20 0">
            <a-image src="#rio_liga" id-src="rio_liga" width="1.18" height="1" geometry="width: 5.63; height: 3.12" btn-click="" liga="/medioambiente/rio" material="" class="activeVR" position="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.7 1.4 0.1"></a-image>
            </a-image>
        </a-entity>

        <a-videosphere id="persona" src="#persona02" geometry="radius: 99; phiLength: 50.03; thetaLength: 85.67; thetaStart: 64.01; phiStart: 66.2" visible="" load-obj="" material=""></a-videosphere>
        
        <a-sky radius="100" src="#bg"  load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/nursery.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>