<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/medioambiente/reforestacion/images/bg.jpg">

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/medioambiente/reforestacion/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="mina_btn1" src="<?= media(); ?>islas/medioambiente/reforestacion/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/medioambiente/reforestacion/images/btn1_press.png">
            <img id="mina_pleca" src="<?= media(); ?>islas/medioambiente/reforestacion/images/pleca.png">
            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="pasoFauna_liga" src="<?= media(); ?>islas/medioambiente/pasosfauna/images/liga.png">
            <img id="pasoFauna_liga_press" src="<?= media(); ?>islas/medioambiente/pasosfauna/images/liga_press.png">
            
            <img id="playa_liga" src="<?= media(); ?>islas/medioambiente/playa/images/liga.png">
            <img id="playa_liga_press" src="<?= media(); ?>islas/medioambiente/playa/images/liga_press.png">


            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-13 -30 -8.2" scale="1 1 1" rotation="0 60 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 8.02; height: 1.54"></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 6.73; height: 1.1" position="0 -0.7 0.2" btn-click="Video">
                <a-image id="check" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.6 0.1"></a-image>
            </a-image>
        </a-entity>

        <a-entity position="-5.5 -25 7.5" rotation="0 140 0">
            <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/medioambiente/playa" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.7 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="12.2 -23 0" rotation="0 275 0">
            <a-image src="#pasoFauna_liga" id-src="pasoFauna_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12"  btn-click="Liga" liga="/medioambiente/pasosfauna">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>

        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 37.07; thetaLength: 65.83; thetaStart: 77; phiStart: 155.82" visible="" load-obj="" material=""></a-videosphere>
        
        <a-sky radius="100" src="#bg" color="#e7edf3" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/medioambiente.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>