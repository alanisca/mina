<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/medioambiente/lab/images/bg.jpeg" crossorigin="anonymous">

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="true" src="<?= media(); ?>islas/medioambiente/lab/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="btn1" src="<?= media(); ?>islas/medioambiente/lab/images/btn1.png" crossorigin="anonymous">
            <img id="btn1_press" src="<?= media(); ?>islas/medioambiente/lab/images/btn1_press.png" crossorigin="anonymous">
            <img id="pleca" src="<?= media(); ?>islas/medioambiente/lab/images/pleca.png" crossorigin="anonymous">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="reforestacion_liga" src="<?= media(); ?>islas/medioambiente/reforestacion/images/liga.png" crossorigin="anonymous">
            <img id="reforestacion_liga_press" src="<?= media(); ?>islas/medioambiente/reforestacion/images/liga_press.png" crossorigin="anonymous">
            <img id="nursery_liga" src="<?= media(); ?>islas/medioambiente/nursery/images/liga.png" crossorigin="anonymous">
            <img id="nursery_liga_press" src="<?= media(); ?>islas/medioambiente/nursery/images/liga_press.png" crossorigin="anonymous">
            <img id="playa_liga" src="<?= media(); ?>islas/medioambiente/playa/images/liga.png" crossorigin="anonymous">
            <img id="playa_liga_press" src="<?= media(); ?>islas/medioambiente/playa/images/liga_press.png" crossorigin="anonymous">

            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png" crossorigin="anonymous">

            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-7 0 -8.2" scale="1 1 1" rotation="-15 60 0" load-obj="opciones">
            <a-image id="pleca" src="#pleca" width="1.18" height="1" geometry="width: 8.5; height: 1.55"></a-image>
            <a-image id="btn01" id-video="persona01" src="#btn1" id-src="btn1" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="0 -.7 0.2" btn-click="Video">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.3 0.5 0.1"></a-image>
            </a-image>
        </a-entity>
        
        <a-videosphere id="persona" src="#persona01" geometry="radius: 99; phiLength: 33.43; thetaLength: 66.96; thetaStart: 72.59; phiStart: 163.28" visible="" load-obj="" material=""></a-videosphere>

        <a-entity position="0.4 4.5 -12.4" rotation="0 30 0">
            <a-image src="#reforestacion_liga" id-src="reforestacion_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" btn-click="Liga" liga="/medioambiente/gigantes" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-3.2 5 -8.5" rotation="0 30 0">
            <a-image src="#nursery_liga" id-src="nursery_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/medioambiente/palaelectrica">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="-9.6 7.2 -8.9" rotation="0 75 0">
            <a-image src="#playa_liga" id-src="playa_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/medioambiente/vuela">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.2 0.4"></a-image>
            </a-image>
        </a-entity>
        
        <a-sky radius="100" src="#bg" color="" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/medioambiente/lab.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>