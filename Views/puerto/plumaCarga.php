<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video regresar">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/puerto/plumacarga/videos/bg.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline></video>
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
        <a-entity id="rig" position="0 1 0" rotation="0 -90 0">
            <a-camera id="camera" camera="fov: 90" look-controls="reverseMouseDrag:true;"></a-camera>
        </a-entity>

        <a-videosphere id="loop01_V" src="" geometry="radius: 99;"></a-videosphere>        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/puerto/plumaCarga.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>