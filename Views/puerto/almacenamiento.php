<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/puerto/almacenamiento/images/bg.jpeg">

            <!-- PERSONA(S) -->
            <video  id="persona01" loop="false" src="<?= media(); ?>islas/puerto/almacenamiento/videos/persona01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona02" loop="false" src="<?= media(); ?>islas/puerto/almacenamiento/videos/persona02.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona03" loop="false" src="<?= media(); ?>islas/puerto/almacenamiento/videos/persona03.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="persona04" loop="false" src="<?= media(); ?>islas/puerto/almacenamiento/videos/persona04.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>
            <video  id="loop01" loop="true" src="<?= media(); ?>islas/puerto/almacenamiento/videos/loop01.mp4" preload="auto" crossorigin="anonymous" webkit-playsinline playsinline> </video>

            <!-- OPCION MULTIPLE -->
            <img id="mina_btn1" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn1_press.png">
            <img id="mina_btn2" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn2.png">
            <img id="mina_btn2_press" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn2_press.png">
            <img id="mina_btn3" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn3.png">
            <img id="mina_btn3_press" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn3_press.png">
            <img id="mina_btn4" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn4.png">
            <img id="mina_btn4_press" src="<?= media(); ?>islas/puerto/almacenamiento/images/btn4_press.png">

            <img id="mina_pleca" src="<?= media(); ?>islas/puerto/almacenamiento/images/pleca.png">
            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">
            
            <!-- LIGAS A OTROS SITIOS -->
            <img id="caribe_liga" src="<?= media(); ?>islas/puerto/caribe/images/liga.png">
            <img id="caribe_liga_press" src="<?= media(); ?>islas/puerto/caribe/images/liga_press.png">
            <img id="plantaenergia_liga" src="<?= media(); ?>islas/puerto/plantaenergia/images/liga.png">
            <img id="plantaenergia_liga_press" src="<?= media(); ?>islas/puerto/plantaenergia/images/liga_press.png">
            <img id="plumacarga_liga" src="<?= media(); ?>islas/puerto/plumacarga/images/liga.png">
            <img id="plumacarga_liga_press" src="<?= media(); ?>islas/puerto/plumacarga/images/liga_press.png">
            <img id="galera_liga" src="<?= media(); ?>islas/puerto/galera/images/liga.png">
            <img id="galera_liga_press" src="<?= media(); ?>islas/puerto/galera/images/liga_press.png">
            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <a-entity id="opciones" position="-6 0 -14" scale="1 1 1" rotation="-15 40 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.18" height="1" geometry="width: 10.46; height: 1.66"></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" width="1.18" height="1" geometry="width: 4.79; height: 1.1" position="-3.8 -0.7 0.2" btn-click="Video" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-2.3 0.5 0.4"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#mina_btn2" id-src="mina_btn2" width="1.18" height="1" geometry="width: 7.43; height: 1.1" position="2.5 -0.7 0.2" btn-click="Video" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.7 0.5 0.4"></a-image>
            </a-image>
            <a-image id="btn03" id-video="persona03" src="#mina_btn3" id-src="mina_btn3" width="1.18" height="1" geometry="width: 6.29; height: 1.1" position="-3 -1.9 0.2" btn-click="Video" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.1 0.6 0.4"></a-image>
            </a-image>
            <a-image id="btn04" id-video="persona04" src="#mina_btn4" id-src="mina_btn4" width="1.18" height="1" geometry="width: 6.63; height: 1.1" position="3.55 -1.9 0.2" btn-click="Video" material="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.4 0.6 0.4"></a-image>
            </a-image>
        </a-entity>
        <a-videosphere id="persona" src="#persona02" geometry="radius: 98; phiLength: 27.48; thetaLength: 66.8; thetaStart: 75.3; phiStart: 185.45" visible="" load-obj="" material=""></a-videosphere>
        <a-videosphere id="loop01_V" src="#loop01" geometry="radius: 99; phiLength: 203.52; thetaLength: 107.87" visible="" load-obj="" material=""></a-videosphere>
        
        <a-entity position="8.1 6.9 1.3" rotation="0 270 0">
            <a-image src="#caribe_liga" id-src="caribe_liga" width="1.18" height="1" geometry="width: 4.93; height: 3.06" material="" btn-click="Liga" liga="/puerto/caribe"></a-image>
        </a-entity>
        <a-entity position="8 5.3 -10" rotation="0 315 0">
            <a-image src="#plantaenergia_liga" id-src="plantaenergia_liga" width="1.18" height="1" geometry="width: 3.91; height: 3.12" material="" btn-click="Liga" liga="/puerto/plantaenergia"></a-image>
        </a-entity>
        <a-entity position="1.1 5 -12.5" rotation="0 -10.3 0">
            <a-image src="#plumacarga_liga" id-src="plumacarga_liga" width="1.18" height="1" geometry="width: 5.25; height: 3.05" material="" btn-click="Liga" liga="/puerto/plumacarga"></a-image>
        </a-entity>
        <a-entity position="-15.02 5.69 2.77" rotation="0 89.45 0">
            <a-image src="#galera_liga" id-src="galera_liga" width="1.18" height="1" geometry="width: 4.82; height: 3.12" material="" btn-click="Liga" liga="/puerto/galera"></a-image>
        </a-entity>

        <a-sky radius="100" src="#bg" load-obj></a-sky>
        
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/puerto/almacenamiento.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 0;
            document.querySelector("#loop01_V").components.material.data.src.currentTime = 0;
        }
    </script>
</body>
</html>