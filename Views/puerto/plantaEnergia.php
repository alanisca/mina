<!DOCTYPE html>
<html lang="es">
<head>
    <?php include('Views/temaplate/header.php'); ?>
</head>
<body class="video">
    <?php include('Views/temaplate/loading.php') ?>
    <?php include('Views/temaplate/mainContain.php') ?>
   
    <a-scene vr-mode-ui="enterVRButton: #vrmode; enabled: true;" load-obj="360Video">
        <a-assets timeout="10000">
            <?php include('Views/temaplate/assets2D.php'); ?>
            <!-- FONDO -->
            <img id="bg" src="<?= media(); ?>islas/puerto/plantaenergia/images/bg.jpg">

            <!-- OPCION MULTIPLE -->
            <img id="mina_btn1" src="<?= media(); ?>islas/puerto/plantaenergia/images/btn1.png">
            <img id="mina_btn1_press" src="<?= media(); ?>islas/puerto/plantaenergia/images/btn1_press.png">
            <img id="mina_btn2" src="<?= media(); ?>islas/puerto/plantaenergia/images/btn2.png">
            <img id="mina_btn2_press" src="<?= media(); ?>islas/puerto/plantaenergia/images/btn2_press.png">

            <img id="mina_pleca" src="<?= media(); ?>islas/puerto/plantaenergia/images/pleca.png">
            <img id="palomita" src="<?= media(); ?>images/home/popup/bottomMenu/palomita.png">
            
            <!-- LIGAS A OTROS SITIOS -->#6667909430*
            <img id="almacenamiento_liga" src="<?= media(); ?>islas/puerto/almacenamiento/images/liga.png">
            <img id="almacenamiento_liga_press" src="<?= media(); ?>islas/puerto/almacenamiento/images/liga_press.png">
            
            <img id="vuelaplantaenergia_liga" src="<?= media(); ?>islas/puerto/vuelaplantaenergia/images/liga.png">
            <img id="vuelaplantaenergia_liga_press" src="<?= media(); ?>islas/puerto/vuelaplantaenergia/images/liga_press.png">


            <?php include('Views/temaplate/menuVRAssets.php'); ?>
        </a-assets>
       
        <!-- <a-entity id="opciones" position="-13 0 -8.2" scale="1 1 1" rotation="0 60 0" load-obj="opciones">
            <a-image id="pleca" src="#mina_pleca" width="1.093" height=".166" geometry="width: 10.93; height: 1.66" material=""></a-image>
            <a-image id="btn01" id-video="persona01" src="#mina_btn1" id-src="mina_btn1" geometry="width: 6.09; height: 1.42" position="-3.37 -0.8 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3.01 0.7 0.1"></a-image>
            </a-image>
            <a-image id="btn02" id-video="persona02" src="#mina_btn2" id-src="mina_btn2" width="1.18" height="1" geometry="width: 6.09; height: 1.43" position="3.09 -0.8 0.2" btn-click="Video" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-3 0.7 0.4"></a-image>
            </a-image>
        </a-entity> -->

        <a-entity position="-5.5 4.5 7.5" rotation="0 140 0">
            <a-image src="#almacenamiento_liga" id-src="almacenamiento_liga" width="1.18" height="1" geometry="width: 3.9; height: 3.12" btn-click="Liga" liga="/puerto/concentradocobre" material="" class="">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.8 1.4 0.1"></a-image>
            </a-image>
        </a-entity>
        <a-entity position="12.2 3.5 0" rotation="0 275 0">
            <a-image src="#vuelaplantaenergia_liga" id-src="vuelaplantaenergia_liga" width="1.18" height="1" geometry="width: 3.92; height: 3.12"  btn-click="Liga" liga="/puerto/vuelaplantaenergia">
                <a-image id="pleca" src="#palomita" width="1" height="1" material="alphaTest: 0.4" geometry="width: 0.4; height: 0.4" position="-1.9 1.4 0.1"></a-image>
            </a-image>
        </a-entity>

        
        <a-sky radius="100" src="#bg" color="#e7edf3" load-obj></a-sky>
        <?php include('Views/temaplate/menuVR.php'); ?>
    </a-scene>
    <script src="<?= media(); ?>js/plugins/jquery.min.js"></script>
    <script src="<?= media(); ?>js/plugins/bootstrap.min.js"></script>
    <script src="<?= media(); ?>js/plugins/howler.min.js"></script>
    <script src="<?= media(); ?>js/data.js"></script>
    <script src="<?= media(); ?>js/loading/animations.js"></script>
    <script src="<?= media(); ?>js/360/main.js"></script>
    <script src="<?= media(); ?>js/360/puerto/plantaEnergia.js"></script>
    <script>
        let init360 = () => {
            document.querySelector("#persona").components.material.data.src.currentTime = 1;
        }
    </script>
</body>
</html>