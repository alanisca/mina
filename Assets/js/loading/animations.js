
if(false){
    $(".loadContain, .plecaContain").css("display", "none");
}
else{
    var home = anime.timeline({
        easing:'linear',
        autoplay: false,
        complete: () => {
            if(home.direction == 'normal'){
                $('.loadContain').css('display','block');
                $('.loadContain').css('background-color','#fff');
            }
            if(home.direction == 'reverse')
                $('.loadContain').css('display','none');
        },
        begin: () => {
            if(home.direction == 'normal'){
                readyScene();
                console.log("readyScene");
                $('.loadContain').css('display','block');
            }
        }
    });
    home.add({
        targets: '.loadContain .contain',
        top: ['-100%','0%'],
        delay: 500,
        duration:1000
    })
    .add({
        targets: '.pleca, .logo',
        opacity: 1,
        duration:1000
    })
    .add({
        targets: '.aros',
        easing: 'easeOutBack',
        scale: [0,1],
    });
    anime({
        targets: '.aros .aroBig',
        easing: 'linear',
        rotate: '360deg',
        duration: 8000,
        loop: true
    });
    anime({
        targets: '.aros .aroSmall',
        easing: 'linear',
        rotate: '-360deg',
        duration: 10000,
        loop: true
    });
    var tapVRLoaded = {opacity:0};
    anime({
        targets: tapVRLoaded,
        opacity: [1,0],
        easing: 'linear',
        duration: 1000,
        loop: true,
        direction: 'alternate',
        update: function(){
            if(document.querySelector('#tap_continuarVR1'))
                document.querySelector('#tap_continuarVR1').setAttribute('opacity',tapVRLoaded.opacity);
        }
    });
    /* VR Animation loading */
    // var homeVR = anime.timeline({
    //     easing:'linear',
    //     autoplay: false,
    //     duration: 4000,
    //     complete: function() {
    //         console.log("complete")
    //         if(homeVR.direction == 'normal'){
    //             document.querySelector('#bgVR').setAttribute("visible","true");
    //             $('.loadContain').css('display','none');
    //         }
    //         if(homeVR.direction == 'reverse')
    //         document.querySelector('#bgVR').setAttribute("visible","false");
    //     },
    //     begin: function() {
    //         console.log("begin")
    //     }
    // });
    var loadPositionVR = {
        top: 1,
        opacity: 0,
        opacity2: 1,
        opacity3: 1,
        scale: 0,
        rotateUp: 0,
        rotateDown: 0
    };
    // homeVR.add({
    //     targets: loadPositionVR,
    //     top: [1,0],
    //     update: function(){
    //         if(document.querySelector('#loadContainVR'))
    //             document.querySelector('#loadContainVR').setAttribute('position',`0 ${loadPositionVR.top} -0.25`);
    //     }
    // })
    // .add({
    //     targets: loadPositionVR,
    //     opacity: [0,1],
    //     scale: [0,1],
    //     update: function(){
    //         if(document.querySelector('#loadContainVR')){
    //             document.querySelector('#aro-bigVR1').setAttribute('opacity',loadPositionVR.opacity);
    //             document.querySelector('#aro-smallVR1').setAttribute('opacity',loadPositionVR.opacity);
    //             document.querySelector('#logoVR1').setAttribute('opacity',loadPositionVR.opacity);
    //             document.querySelector('#aro-bigVR1').setAttribute('scale',`${loadPositionVR.scale} ${loadPositionVR.scale} 1`);
    //             document.querySelector('#aro-smallVR1').setAttribute('scale',`${loadPositionVR.scale} ${loadPositionVR.scale} 1`);
    //             document.querySelector('#logoVR1').setAttribute('scale',`${loadPositionVR.scale} ${loadPositionVR.scale} 1`);
    //         }
    //     }
    // })
    // .add({
    //     targets: loadPositionVR,
    //     opacity2: [1,0],
    //     update: function(){
    //         if(document.querySelector('#loadContainVR')){
    //             document.querySelector('#plecaCargandoVR1').setAttribute('opacity',loadPositionVR.opacity2);
    //             document.querySelector('#aro-bigVR1').setAttribute('opacity',loadPositionVR.opacity2);
    //             document.querySelector('#aro-smallVR1').setAttribute('opacity',loadPositionVR.opacity2);
    //         }
    //     }
    // })
    // .add({
    //     targets: loadPositionVR,
    //     opacity3: [0,1],
    //     update: function(){
    //         if(document.querySelector('#loadContainVR')){
    //             document.querySelector('#pleca_nombreVR1').setAttribute('opacity',loadPositionVR.opacity3);
    //         }
    //     }
    // });
    anime({
        targets: loadPositionVR,
        easing: 'linear',
        rotateUp: [0,360],
        duration: 8000,
        loop: true,
        update: function(){
             if(document.querySelector('#loadContainVR')){
                document.querySelector('#aro-bigVR1').setAttribute('rotation',`0 0 ${loadPositionVR.rotateUp}`);
             }
        }
    });
    anime({
        targets: loadPositionVR,
        easing: 'linear',
        rotateDown: [0,-360],
        duration: 10000,
        loop: true,
        update: function(){
             if(document.querySelector('#loadContainVR')){
                document.querySelector('#aro-smallVR1').setAttribute('rotation',`0 0 ${loadPositionVR.rotateDown}`);
             }
        }
    });
}
let closeLoading = (liga = '') => {
    if(false){
        // homeVR.reverse();
        // homeVR.play();
        // if(homeVR.direction == 'reverse')
        //         document.querySelector('#bgVR').setAttribute("visible","false");
    }
    else{
        home.reverse();
        home.play();
        if(home.direction == 'reverse'){
            $('.loadContain').css('background-color','rgb(255 255 255 / 0%)');
        }
    }
    setTimeout(() => {
        init360();
    }, 50);
    if(liga != ''){
        home.finished.then(goIsla360(host+liga));
    }
}
let isReady;
$(function() {
    if(false) {
        // console.log("vrMode"+vrMode)
        // setTimeout(() => {
            // homeVR.play();
        // }, 1000);
    }
    else {
        console.log("vrMode"+vrMode);
        home.play();
    }
    isReady = () => {
        console.log("verificando: ",paceLoaded,isLoaded,"=",isLoaded*paceLoaded);
        console.log("Activar camara",vrMode);
        if(vrMode){
            // $('#camera').attr("active","true");
        }
        else{
            
        }
        if(vrMode){
            // document.getElementById('normalCamera').setAttribute("active","true");
            document.getElementById('vrCameraParent').setAttribute("visible","true");
            document.querySelector('a-scene').setAttribute('cursor','rayOrigin','entity');
            console.log("true");
            if(isLoaded*paceLoaded){ /* Verifica que ya se cargó todo */
                if(home.completed) closeLoading(); /* Si la animación ya termino sube la cortinilla */
                else{
                    home.finished.then(closeLoading); /* Si aún no termina, se espera a que termine para subirla */
                }
                $(".plecaContain .instrucciones").css("display","none");
                // homeVR.play();
                // if(homeVR.completed) closeLoading(); /* Si la animación ya termino sube la cortinilla */
                // else{
                //     homeVR.finished.then(closeLoading); /* Si aún no termina, se espera a que termine para subirla */
                // }
                clearInterval(intervalID);
                /* Se activa camara correcta */
                document.querySelector('#normalCamera').setAttribute('camera','active','true');
            }
        }
        else{
            // document.getElementById('normalCamera').setAttribute("active","false");
            document.getElementById('vrCameraParent').setAttribute("visible","false");
            document.querySelector('a-scene').setAttribute('cursor','rayOrigin','mouse');
            console.log("false");
            if(isLoaded*paceLoaded){ /* Verifica que ya se cargó todo */
                if(home.completed) closeLoading(); /* Si la animación ya termino sube la cortinilla */
                else{
                    home.finished.then(closeLoading); /* Si aún no termina, se espera a que termine para subirla */
                }
                // init360();
                clearInterval(intervalID);
                /* Se activa camara correcta */
                // document.querySelector('#camera').setAttribute('camera','active','true');
            }
        }
        return isLoaded*paceLoaded;
    }
    var intervalID = setInterval(isReady, 1000);
});