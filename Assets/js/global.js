/* Para iniciar los elementos 3D */
var totalLoaded = 0;
var isLoaded = false;
var vrMode = false;
var vrMenuActive = false;
if(localStorage.getItem("vrMode")){
    vrMode = (localStorage.getItem("vrMode") === 'true');
}
AFRAME.registerComponent('load-obj', {
    init: function () {
        this.el.addEventListener("loaded", () => {
            totalLoaded++;
            if(totalLoaded == document.querySelectorAll('[load-obj]').length){
                isLoaded = true;
                if(localStorage.getItem("vrMode") == "true") {
                    $('.instrucciones').css({
                        "opacity": 0,
                        "display": "none"
                    });
                    document.querySelector('a-scene').enterVR();
                }
            }
        });
    }
});
let inVRMode = (type) => {
    vrMode = true;
    localStorage.setItem("vrMode", "true");
    $(".vrModeObj").attr("visible","true");
    $(".normalModeObj").attr("visible","false");
    $('.plecaContain').css({
        "opacity": 1,
        "display": "flex"
    });
    $('.plecaConmainContainain').css({
        "dsiplay": "none"
    });
    if(type == "360"){}
    if(type == "home"){
        document.querySelector('#menu #menuVR a-image[id-src="menuVR_islas"]').setAttribute("visible","false");
        let isla_comuEL = document.querySelector('.isla_comuParent');
        let isla_medioEL = document.querySelector('.isla_medioParent');
        let isla_minaEL = document.querySelector('.isla_minaParent');
        let isla_procesEL = document.querySelector('.isla_procesParent');
        let isla_puertoEL = document.querySelector('.isla_puertoParent');
        isla_comuEL.setAttribute('rotation', `0 324 0`);
        isla_medioEL.setAttribute('rotation', `0 36 0`);
        isla_minaEL.setAttribute('rotation', `0 108 0`);
        isla_procesEL.setAttribute('rotation', `0 252 0`);
        isla_puertoEL.setAttribute('rotation', `0 180 0`);
    }
    /* Quitando elementos 2D */
    $('a-scene').css("z-index","3");
    /* VRCAMERA */
    // $("a-ring.vrCursor").attr("visible","true");
    // document.querySelector('#vrCursor').setAttribute("cursor","rayOrigin","entity");
    // document.getElementById('vrCameraParent').setAttribute("active","true");
    document.getElementById('normalCamera').setAttribute("active","false");
    document.getElementById('vrCameraParent').setAttribute("visible","true");
    document.querySelector('a-scene').setAttribute('cursor','rayOrigin','entity');
}
let outVRMode = () => {
    vrMode = false;
    // if(home.direction == 'reverse'){
    //     home.reverse();
    //     home.play();
    //     isReady();
    // }
    $('.plecaContain').css({
        "opacity": 0,
        "display": "none"
    });
    $('.plecaConmainContainain').css({"dsiplay": "flex"});
    // document.querySelector('#vrCursor').setAttribute("cursor","rayOrigin","mouse");
    // document.querySelector('#vrCursor').setAttribute("visible","false");
    // $("#camera").attr("camera","active:true;");
    // $("#VRCamera").attr("camera","active:false;");
    localStorage.setItem("vrMode", "false");
    $(".vrModeObj").attr("visible","false");
    $(".normalModeObj").attr("visible","true");
    /* Colocando elementos 2D */
    $('a-scene').css("z-index","0");
    /* VRCAMERA */
    // $("a-ring.vrCursor").attr("visible","false");
    // document.querySelector('#vrCursor').setAttribute("cursor","rayOrigin","mouse");
    document.getElementById('normalCamera').setAttribute("active","false");
    document.getElementById('vrCameraParent').setAttribute("visible","false");
    document.querySelector('a-scene').setAttribute('cursor','rayOrigin','mouse');
    // document.getElementById('normalCamera').setAttribute("cursor","rayOrigin","mouse");
}