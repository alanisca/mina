AFRAME.registerComponent('fcamera', {
    init: function(){
        
    },
    tick: function () {
        if(!vrMode){
            let rotation = this.el.getAttribute('rotation');
            let isla_comuEL = document.querySelector('.isla_comuParent');
            let isla_medioEL = document.querySelector('.isla_medioParent');
            let isla_minaEL = document.querySelector('.isla_minaParent');
            let isla_procesEL = document.querySelector('.isla_procesParent');
            let isla_puertoEL = document.querySelector('.isla_puertoParent');
            isla_comuEL.setAttribute('rotation', `0 ${rotation.y} 0`);
            isla_medioEL.setAttribute('rotation', `0 ${rotation.y} 0`);
            isla_minaEL.setAttribute('rotation', `0 ${rotation.y} 0`);
            isla_procesEL.setAttribute('rotation', `0 ${rotation.y} 0`);
            isla_puertoEL.setAttribute('rotation', `0 ${rotation.y} 0`);
        }
        else{
            let rotation = this.el.getAttribute('rotation');
            let isla_comuEL = document.querySelector('.isla_comuParent');
            let isla_medioEL = document.querySelector('.isla_medioParent');
            let isla_minaEL = document.querySelector('.isla_minaParent');
            let isla_procesEL = document.querySelector('.isla_procesParent');
            let isla_puertoEL = document.querySelector('.isla_puertoParent');
            isla_comuEL.setAttribute('rotation', `0 324 0`);
            isla_medioEL.setAttribute('rotation', `0 36 0`);
            isla_minaEL.setAttribute('rotation', `0 108 0`);
            isla_procesEL.setAttribute('rotation', `0 252 0`);
            isla_puertoEL.setAttribute('rotation', `0 180 0`);
        }
    }
});