/*Solo en islas*/
$(document).ready(function(){
    
    document.querySelector('a-scene').addEventListener('enter-vr', function () {
        inVRMode("home");
        document.querySelector('#normalCamera').setAttribute('camera','active','true');
    });
    document.querySelector('a-scene').addEventListener('exit-vr', function () {
        outVRMode();
        document.querySelector('#camera').setAttribute('camera','active','true');
    });
});