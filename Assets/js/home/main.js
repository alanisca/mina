var closeMenu = new Howl({src: [`${host}/Assets/islas/procesos/audio/closeMenu.mp3`]});
var fadeInFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fadeInFases.mp3`]});
var fadeOutFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fadeOutFases.mp3`]});
var fateTooggleFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fateTooggleFases.mp3`]});
var playButton = new Howl({src: [`${host}/Assets/islas/procesos/audio/playButton.mp3`]});
var playIsla = new Howl({src: [`${host}/Assets/islas/procesos/audio/playIsla.mp3`]});
$(document).ready(function(){
    if(vrMode){
        // $('#camera').attr("active","true");
        $('#vrCameraParent').attr("active","false");
        $('#vrCameraParent').attr("visible","false");
        console.log("true");
    }
    else{
        $('#vrCameraParent').attr("active","true");
        $('#vrCameraParent').attr("visible","true");
        console.log("false");

    }
    $('.comenzar').on('click', function(){
        anime({
            targets:'.contain.introduccion',
            scale: [1,0],
            easing: 'easeInBack',
            duration:1000,
            complete: function(){
                $('.contain.introduccion').css('display','none');
                anime({
                    targets: '.contain.instrucciones',
                    scale: [0,1],
                    easing: 'easeOutBack',
                    duration: 1000,
                    begin: function(){
                        $('.contain.instrucciones').css('display','flex');
                    }
                });
            },
            begin: function(){
                playButton.play();
            }
        })
    });
    $('.entendido').on('click', function(){
        anime({
            targets:'.contain.instrucciones',
            scale: [1,0],
            easing: 'easeInBack',
            duration:1000,
            complete: function(){
                $('.contain.instrucciones').css('display','none');
                anime({
                    targets: '.plecaContain',
                    opacity: [1,0],
                    easing: 'linear',
                    duration: 500,
                    complete: function(){
                        $('.plecaContain').css('display','none');
                    }
                });
            },
            begin: function(){
                playButton.play();
            }
        })
    });
    $('.menu').on('click',function(){
        anime({
            targets: '.plecaContain',
            opacity: [0,1],
            easing: 'linear',
            duration: 500,
            begin: function(){
                playButton.play();
                $('.plecaContain').css('display','flex');
                anime({
                    targets: '.contain.menus',
                    scale: [0,1],
                    easing: 'easeOutBack',
                    duration: 1000,
                    begin: function(){
                        $('.contain.menus').css('display','flex');
                    }
                });
            },
        });
    });
    $('.plecaContain .menus .cerrar').on('click',function(){
        closeMenu.play();
        anime({
            targets: '.contain.menus',
            scale: [1,0],
            easing: 'easeInBack',
            duration: 1000,
            complete: function(){
                $('.contain.menus').css('display','none');
                anime({
                    targets: '.plecaContain',
                    opacity: [1,0],
                    easing: 'linear',
                    duration: 500,
                    complete: function(){
                        $('.plecaContain').css('display','none');
                    }
                });
            }
        });
    });
    let bottomMenu = () => {
        if($('.bottomMenu').hasClass('open')) fadeInFases.play();
        else fadeOutFases.play();
        $('.bottomMenu').toggleClass('open');
        console.log("menu");
    }
    $('.mainContain .bottomMenu .boton').on('click', bottomMenu);
    
    let progresoHistoria = ($obj) => {
        fateTooggleFases.play();
        $('.mainContain .bottomMenu .step').removeClass('active');
        $obj.addClass('active');
        $('.mainContain .bottomMenu h3').text(`Etapa ${$obj.attr('data-index')} - ${data.historia[($obj.attr('data-index') - 1)].titulo}`)
    }
    $('.mainContain .bottomMenu .step').on('click', function(){progresoHistoria($(this))});
    
    let silenciar = () => {
        $('.mainContain .footer .audioButton').toggleClass('off');
    }
    $('.mainContain .footer .audioButton').on('click',silenciar);

    // let enterVRmode = () => {
    //     console.log("Hola VR");
    //     document.querySelector('a-scene').enterVR();
    // }
    // $('.mainContain .footer .vrButton').on('click',enterVRmode);
    /* Elimina esfera de tap en el Home */
    $('#tapSky').attr("scale","0 0 0");
});
let goIsla360 = (liga) => {
    playIsla.play();
    window.location.href = liga;
    playIsla.on('end', function(){
        console.log('Finished!');
    });
}
let readyScene = () =>{
    
}
// var cursorVRArr = {cursor: 0};
// let cursorIn = anime({
//     targets: cursorVRArr,
//     cursor: [0,360],
//     easing: 'linear',
//     update: function(){
//         document.querySelector('#cursor').setAttribute('geometry','thetaLength',cursorVRArr.cursor);
//     }
// });
