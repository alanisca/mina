const loader = new THREE.FileLoader();
THREE.Cache.enabled = true;
var totalVideo = 0;
var totalLoad = 0;
var totalLoadedFile = 0;
var loading = 0;
var arrPreload = [];

function loadFiles(file){
    loader.load(
        file,
        function ( data ){
            totalLoadedFile++;
            // console.log(loading+"% "+totalLoadedFile+" de "+$("a-assets *").length);
            if(loading >= 99 || (totalLoadedFile+1) == $("a-assets *").length) {
                setTimeout(() => {
                    $(".loadGame").fadeOut(1000);
                }, 2000);
            }
        },
        function ( xhr ) {
            // console.log((xhr.loaded / xhr.total * 100) + '% loaded' );
            totalLoad = parseInt((xhr.loaded / xhr.total * 100).toFixed());
            loading = (totalLoad/totalVideo)+((totalLoadedFile*(100/totalVideo)));
            // console.log("Cargando: "+loading);
        },
        function ( err ) {
            // console.log("Error: "+err);
        }
    );
}
async function run(){
    if($("a-assets *").length > 0){
        for (let index = 0; index < $("a-assets *").length; index++) {
            totalVideo++;
            // console.log($("a-assets *")[index].getAttribute("src"));
            await loadFiles($("a-assets *")[index].getAttribute("src"));
        }
    }
    else{
        setTimeout(() => {
            $(".loadGame").fadeOut(1000);
        }, 2000);
    }
}
run();