var closeMenu = new Howl({src: [`${host}/Assets/islas/procesos/audio/closeMenu.mp3`]});
var fadeInFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fadeInFases.mp3`]});
var fadeOutFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fadeOutFases.mp3`]});
var fateTooggleFases = new Howl({src: [`${host}/Assets/islas/procesos/audio/fateTooggleFases.mp3`]});
var playButton = new Howl({src: [`${host}/Assets/islas/procesos/audio/playButton.mp3`]});
var playIsla = new Howl({src: [`${host}/Assets/islas/procesos/audio/playIsla.mp3`]});
$(document).ready(function(){
   
    if($('body').hasClass('video')){
        $('.contain.introduccion').css({
            'transform': 'scale(0)',
            'display': 'none'
        });
        $('.contain.instrucciones').css({
            'transform': 'scale(1)',
            'display': 'flex'
        });
        // document.querySelector('#plecaVR1').setAttribute("scale","0 0 0");
        $('h5.instruccion1').html('Gira el dispositivo a tu alrededor.');
        $('h5.instruccion2').html('Toca los botones de diálogo para conocer más o los botones sobre el escenario para cambiar de lugar.');
    }
    $('.comenzar').on('click', function(){
        anime({
            targets:'.contain.introduccion',
            scale: [1,0],
            easing: 'easeInBack',
            duration:1000,
            complete: function(){
                $('.contain.introduccion').css('display','none');
                anime({
                    targets: '.contain.instrucciones',
                    scale: [0,1],
                    easing: 'easeOutBack',
                    duration: 1000,
                    begin: function(){
                        $('.contain.instrucciones').css('display','flex');
                    }
                });
            },
            begin: function(){
                playButton.play();
            }
        })
    });
    $('.entendido').on('click', function(){
        anime({
            targets:'.contain.instrucciones',
            scale: [1,0],
            easing: 'easeInBack',
            duration:1000,
            complete: function(){
                $('.contain.instrucciones').css('display','none');
                anime({
                    targets: '.plecaContain',
                    opacity: [1,0],
                    easing: 'linear',
                    duration: 500,
                    complete: function(){
                        $('.plecaContain').css('display','none');
                    }
                });
            },
            begin: function(){
                playButton.play();
            }
        })
    });
    $('.menu').on('click',function(){
        anime({
            targets: '.plecaContain',
            opacity: [0,1],
            easing: 'linear',
            duration: 500,
            begin: function(){
                playButton.play();
                $('.plecaContain').css('display','flex');
                anime({
                    targets: '.contain.menus',
                    scale: [0,1],
                    easing: 'easeOutBack',
                    duration: 1000,
                    begin: function(){
                        $('.contain.menus').css('display','flex');
                    }
                });
            },
    
        });
    });
    $('.plecaContain .menus .cerrar').on('click',function(){
        closeMenu.play();
        anime({
            targets: '.contain.menus',
            scale: [1,0],
            easing: 'easeInBack',
            duration: 1000,
            complete: function(){
                $('.contain.menus').css('display','none');
                anime({
                    targets: '.plecaContain',
                    opacity: [1,0],
                    easing: 'linear',
                    duration: 500,
                    complete: function(){
                        $('.plecaContain').css('display','none');
                    }
                });
            }
        });
    });
    // let bottomMenu = () => {
    //     $('.bottomMenu').toggleClass('open');
    // }
    // $('.mainContain .bottomMenu .boton').on('click', bottomMenu);
    
    let progresoHistoria = ($obj) => {
        fateTooggleFases.play();
        $('.mainContain .bottomMenu .step').removeClass('active');
        $obj.addClass('active');
        $('.mainContain .bottomMenu h3').text(`Etapa ${$obj.attr('data-index')} - ${data.historia[($obj.attr('data-index') - 1)].titulo}`)
    }
    $('.mainContain .bottomMenu .step').on('click', function(){progresoHistoria($(this))});
    
    let silenciar = () => {
        $('.mainContain .footer .audioButton').toggleClass('off');
        if($('.mainContain .footer .audioButton').hasClass('off')) Howler.volume(0)
        else Howler.volume(1)
    }
    $('.mainContain .footer .audioButton').on('click',silenciar);
    let enterVRmode = () => {
        document.querySelector('a-scene').enterVR();
    }
    $('.mainContain .footer .vrButton').on('click',enterVRmode);
    document.querySelector('a-scene').addEventListener('enter-vr', function () {
        inVRMode("360");
    });
    document.querySelector('a-scene').addEventListener('exit-vr', function () {
        outVRMode();
    });
});

let goIsla360 = (liga) => {
    // return false;
    playIsla.play();
    window.location.href = liga;

}
var taptoCont = {opacity: 1};
let exitTapSky = () => {
    anime({
        targets: taptoCont,
        opacity: [1,0],
        easing: 'linear',
        autoplay: true,
        duration: 1000,
        update: function() {
            $('#tapSky').attr('material','opacity:'+taptoCont.opacity+';');
        },
        complete: function (){
            $('#tapSky').attr("scale","0 0 0");
        }
    });
}
$('.regresar-islas, .plecaContain .menus .regresarIsla').on('click', function(){regresarIslas()});
$('body.regresar .regresar-atras').on('click', function(){window.history.back();});
$('.plecaContain .menus .how').on('click', comoSeUsa);
$('.plecaContain .menus .reiniciar').on('click', reiniciar360);
$('.plecaContain .menus .acercade').on('click', acercade);