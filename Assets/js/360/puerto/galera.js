var ambiente = new Howl({src: [`${host}/Assets/islas/puerto/galera/audio/ambiente.mp3`],volume: .5, loop: true});
var persona01 = new Howl({src: [`${host}/Assets/islas/puerto/galera/audio/persona01.mp3`],volume: 1, loop: false});
$('.plecaContain .instrucciones .entendido').on('click', function(){
    readyClickScene();
});
$('.plecaContain').on('click', function(){
    if(vrMode) readyClickScene();
});
let readyScene = () =>{
    document.getElementById('rig').setAttribute('rotation','0 90 0');
    document.getElementById("loop01_V").setAttribute("src","#loop01");
}
let readyClickScene = () => {
    exitTapSky();
    document.getElementById('loop01').play();
    ambiente.play();
    persona01.play();
}

document.getElementById('vrCameraParent').setAttribute('visible','false');