var ambiente = new Howl({src: [`${host}/Assets/islas/puerto/plumacarga/audio/ambiente.mp3`],volume: .5, loop: true});
var voz = new Howl({src: [`${host}/Assets/islas/puerto/plumacarga/audio/voz.mp3`],volume: .5, loop: false});
$('.plecaContain .instrucciones .entendido').on('click', function(){
    readyClickScene();
});
$('.plecaContain').on('click', function(){
    if(vrMode) readyClickScene();
});
let readyScene = () =>{
    document.getElementById('vrCameraParent').setAttribute('rotation','0 90 0');
    document.getElementById("loop01_V").setAttribute("src","#loop01");
}
let readyClickScene = () => {
    exitTapSky();
    document.getElementById('loop01').play();
    ambiente.play();
    voz.play();
}