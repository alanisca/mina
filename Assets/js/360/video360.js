
/* Animación Camera */
var cursorVRArr = {cursor: 0};
let cursorIn = anime({
    targets: cursorVRArr,
    cursor: [0,360],
    easing: 'linear',
    update: function(){
        if(true){
            if(document.querySelector('#cursor-load'))
                document.querySelector('#cursor-load').setAttribute('geometry','thetaLength',cursorVRArr.cursor);
        }
    },
    complete:function(){
        if(vrMode){
            if(tipoBoton = document.querySelector('.activeVR')){
                var tipoBoton = document.querySelector('.activeVR').getAttribute('btn-click');
                console.log(tipoBoton);
            }
            if(tipoBoton == "Liga"){
                if(document.querySelector('.activeVR'))
                    var ligaBoton = document.querySelector('.activeVR').getAttribute('liga');
                goIsla360(host+ligaBoton);
            }
            if(tipoBoton == "Video"){
                if(document.querySelector('.activeVR')){
                    if(document.querySelector("#persona").getAttribute('src') != '')
                        document.querySelector("#persona").components.material.material.map.image.pause();              
                    var playVideo = document.querySelector('.activeVR').getAttribute('id-video');
                    console.log(playVideo);
                    document.getElementById(playVideo).pause();
                    document.getElementById(playVideo).currentTime = .1;
                    document.getElementById(playVideo).play();
                    $('#persona').attr('src','#'+playVideo);
                }
            }
            if(tipoBoton == "Menu"){
                if(document.querySelector('#menuVR').getAttribute('visible')){
                    vrMenuActive = false;
                    document.querySelector('#menuVR').setAttribute('visible','false')
                    document.querySelector('#menuVR').setAttribute('scale','0 0 0');
                }
                else{
                    vrMenuActive = true;
                    document.querySelector('#menuVR').setAttribute('visible','true')
                    document.querySelector('#menuVR').setAttribute('scale','1 1 1');
                }
            }
            if(tipoBoton == "MenuBoton"){
                if(document.querySelector('.activeVR')){
                    var menuBoton = document.querySelector('.activeVR').getAttribute('id-src');
                    if(menuBoton == "menuVR_salirvr") document.querySelector('a-scene').exitVR();
                }
                document.querySelector('#menuVR').setAttribute('visible','false')
                document.querySelector('#menuVR').setAttribute('scale','0 0 0');
            }
        }
    }
});

var playVideo;
var tipoBoton;
var ligaBoton;
var paceLoaded = false;
Pace.on('done', function() {
    paceLoaded = true;
});
/* Botones a elementos 3D */
AFRAME.registerComponent('btn-click', {
    schema: {type: 'string'},
    init: function () {
        /* Entra al área de un animal */
        this.el.addEventListener('mouseenter', function (evt) {
            console.log("mouseenter");
            document.querySelectorAll('.vr-button').forEach((obj) => {
                obj.setAttribute('src','#'+obj.getAttribute("id-src"));
            });
            this.setAttribute('src','#'+this.getAttribute("id-src")+'_press');
            if(vrMode){
            // if(true){
                this.classList.add("activeVR");
                this.setAttribute('src','#'+this.getAttribute("id-src")+'_press');
                cursorIn.play();
            }
        });
        /* Sale del área de un animal  */
        this.el.addEventListener('mouseleave', function (evt) {
            console.log("mouseleave");
            this.setAttribute('src','#'+this.getAttribute("id-src"));
            if(vrMode){
            // if(true){
                this.classList.remove("activeVR");
                this.setAttribute('src','#'+this.getAttribute("id-src"));
                cursorIn.restart();
                cursorIn.pause();
                document.querySelector('#cursor-load').setAttribute('geometry','thetaLength',0);
            }
        });
        this.el.addEventListener('click', function (evt) {
            console.log("click");
            // return false;//no saltar de pagina
            if(!vrMode){
                tipoBoton = this.getAttribute('btn-click');
                if(tipoBoton == "Video"){
                    document.querySelector("#persona").components.material.material.map.image.pause();              
                    playVideo = this.getAttribute('id-video');
                    document.getElementById(playVideo).pause();
                    document.getElementById(playVideo).currentTime = .1;
                    document.getElementById(playVideo).play();
                    $('#persona').attr('src','#'+playVideo);
                }
                if(tipoBoton == "Liga"){
                    ligaBoton = this.getAttribute('liga');
                    console.log(host+ligaBoton);
                    goIsla360(host+ligaBoton);
                }
            }
        });
    },
    tick: function () {
        /*Menu rota con la camara si esta activo*/
        // if(vrMode){
        //     if(!vrMenuActive){
        //         document.querySelector('#menu').setAttribute('rotation',`0 ${document.querySelector('#camera').getAttribute('rotation').y} 0`);
        //     }
        // }
    }
});
/* Regresar a las islas */
let regresarIslas = () =>{
    window.location.href = host;
}
let comoSeUsa = () =>{
    anime({
        targets: '.contain.menus',
        scale: [1,0],
        easing: 'easeInBack',
        duration: 1000,
        complete: function(){
            $('.contain.menus').css('display','none');
            anime({
                targets: '.contain.instrucciones',
                scale: [0,1],
                easing: 'linear',
                duration: 500,
                begin: function(){
                    $('.contain.instrucciones').css('display','flex');
                }
            });
        }
    });
    //Quitar Menú
    //Aparecer menú de como se usa
}
let reiniciar360 = () =>{
    //borrar localStorage
    //Regresar a las islas
}
let acercade = () =>{
    //Quitar Menú
    //Aparecer menú de acerca de
}