var ambiente = new Howl({src: [`${host}/Assets/islas/procesos/molienda/audio/ambiente.mp3`],volume: .5, loop: true});
$('.plecaContain .instrucciones .entendido').on('click', function(){
    readyClickScene();
});
$('.plecaContain').on('click', function(){
    if(vrMode) readyClickScene();
});

let readyScene = () =>{
    document.getElementById('vrCameraParent').setAttribute('rotation','0 -90 0');
    
    document.getElementById("loop01_V").setAttribute("src","#loop01");
    document.getElementById("loop02_V").setAttribute("src","#loop02");
    document.getElementById("loop03_V").setAttribute("src","#loop03");
    document.getElementById("persona").setAttribute("src","#video01");

    document.getElementById('loop01').play();
    document.getElementById('loop02').play();
    document.getElementById('loop03').play();
}
let readyClickScene = () => {
    exitTapSky();
    document.getElementById('video01').play();
    document.getElementById('video01').pause();
    ambiente.play();
}