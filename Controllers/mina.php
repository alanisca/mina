<?php 
	class mina extends Controllers{
		public function __construct()
		{
			parent::__construct();
		}
		public function mina(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Mina";
			$data['page_title'] = "Mina";
			$data['page_name'] = "mina";
			$this->views->getView($this,"mina",$data);
		}
		public function molienda(){
			$this->views->getView($this,"molienda");
		}
		public function gigantes(){
			$this->views->getView($this,"gigantes");
		}
		public function camion(){
			$this->views->getView($this,"camion");
		}
		public function palaelectrica(){
			$this->views->getView($this,"palaelectrica");
		}
		public function vuela(){
			$this->views->getView($this,"vuela");
		}
		public function simulacion(){
			$this->views->getView($this,"simulacion");
		}
	}
 ?>