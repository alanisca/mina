<?php 
	class medioambiente extends Controllers{
		public function __construct()
		{
			parent::__construct();
		}
		public function medioambiente(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Medioambiente";
			$data['page_title'] = "Medio ambiente";
			$data['page_name'] = "Medio Ambiente";
			$this->views->getView($this,"medioambiente",$data);
		}
		public function playa(){
			$this->views->getView($this,"playa");
		}
		public function pitoffice(){
			$this->views->getView($this,"pitoffice");
		}
		public function lab(){
			$this->views->getView($this,"lab");
		}
		public function nursery(){
			$this->views->getView($this,"nursery");
		}
		public function rio(){
			$this->views->getView($this,"rio");
		}
		public function pasosfauna(){
			$this->views->getView($this,"pasosfauna");
		}
	}
 ?>