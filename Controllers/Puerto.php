<?php 
	class puerto extends Controllers{
		public function __construct()
		{
			parent::__construct();
		}
		public function puerto(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Puerto";
			$data['page_title'] = "Puerto";
			$data['page_name'] = "Puerto";
			$this->views->getView($this,"almacenamiento",$data);
		}
		public function galera(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Galera";
			$data['page_title'] = "Galera";
			$data['page_name'] = "Galera";
			$this->views->getView($this,"galera",$data);
		}
		public function caribe(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Caribe";
			$data['page_title'] = "Caribe";
			$data['page_name'] = "Caribe";
			$this->views->getView($this,"caribe",$data);
		}
		public function plantaEnergia(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Planta de energía";
			$data['page_title'] = "Planta de energía";
			$data['page_name'] = "Planta de energía";
			$this->views->getView($this,"plantaEnergia",$data);
		}
		public function plumaCarga(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Planta de energía";
			$data['page_title'] = "Planta de energía";
			$data['page_name'] = "Planta de energía";
			$this->views->getView($this,"plumaCarga",$data);
		}
	}
 ?>