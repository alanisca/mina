<?php 
	class comunidades extends Controllers{
		public function __construct()
		{
			parent::__construct();
		}
		public function comunidades(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Comunidades";
			$data['page_title'] = "Comunidades vecinas";
			$data['page_name'] = "Comunidades vecinas";
			$this->views->getView($this,"parque",$data);
		}
		public function iptcoclecito(){
			$this->views->getView($this,"iptcoclecito");
		}
		public function parque(){
			$this->views->getView($this,"parque");
		}
		public function healthcenter(){
			$this->views->getView($this,"healthcenter");
		}
		public function defensoresHidricos(){
			$this->views->getView($this,"defensoresHidricos");
		}
		public function ceibaFarm(){
			$this->views->getView($this,"ceibaFarm");
		}
		public function finca(){
			$this->views->getView($this,"finca");
		}
		public function caminos(){
			$this->views->getView($this,"caminos");
		}
		public function historia(){
			$this->views->getView($this,"historia");
		}
		public function testimonio(){
			$this->views->getView($this,"testimonio");
		}
	}
 ?>