<?php 
	class procesamiento extends Controllers{
		public function __construct()
		{
			parent::__construct();
		}
		public function procesamiento(){
			$data['page_id'] = 1.1;
			$data['page_tag'] = "Procesamiento";
			$data['page_title'] = "Procesamiento de recursos";
			$data['page_name'] = "procesamiento";
			$this->views->getView($this,"procesamiento",$data);
		}
		public function molienda(){
			$this->views->getView($this,"molienda");
		}
		public function flotacion(){
			$this->views->getView($this,"flotacion");
		}
		public function centroControl(){
			$this->views->getView($this,"centroControl");
		}
		public function seguridad(){
			$this->views->getView($this,"seguridad");
		}
		public function eppmina(){
			$this->views->getView($this,"eppmina");
		}
		public function drone(){
			$this->views->getView($this,"drone");
		}
		public function relaves(){
			$this->views->getView($this,"relaves");
		}
		public function areanorte(){
			$this->views->getView($this,"areanorte");
		}
		public function vuelarelave(){
			$this->views->getView($this,"vuelarelave");
		}
	}
 ?>